open Batteries
open BatList
open Elang
open Rtl
open Ltl
open Ltl_print
open Utils
open Prog
open Options
open Archi

(* This file performs the translation from LTL programs to RISC-V assembly
   programs. The languages are basically the same, so the only thing to do here
   is to print LTL instructions in RISC-V assembly syntax.

   Another thing to do is to deal with command-line arguments. These are given
   as strings and need to be converted into strings. That is the purpose of the
   [riscv_load_args] function in this file.

   Finally, [riscv_prelude] gives an entry point in the program : a global
   "_start" symbol is created, which points to startup code, whose job is the
   following:
   - initializes the global pointer (for "heap" allocation),
   - sets up the stack,
   - retrieves arguments,
   - calls the "main" function of our program,
   - prints the return value to the screen,
   - and finally exits the program by issuing the appropriate system call.
*)

let riscv_of_cmp (cmp : rtl_cmp) =
  match cmp with
  | Rtl.Rclt -> "blt"
  | Rtl.Rcle -> "ble"
  | Rtl.Rcgt -> "bgt"
  | Rtl.Rcge -> "bge"
  | Rtl.Rceq -> "beq"
  | Rtl.Rcne -> "bne"

let print_binop (b : binop) =
  match b with
  | Elang.Eadd -> "add"
  | Elang.Emul -> "mul"
  | Elang.Emod -> "remu"
  | Elang.Exor -> "xor"
  | Elang.Ediv -> "divu"
  | Elang.Esub -> "sub"
  | Elang.Eor -> "or"
  | Elang.Eand -> "and"
  | Elang.Eband -> "and"
  | _ -> failwith "Unexpected binop"

let print_unop (u : unop) = match u with Elang.Eneg -> "neg" | Enot -> "seqz"

let instrsuffix_of_size sz =
  match sz with MAS1 -> 'b' | MAS4 -> 'w' | MAS8 -> 'd'

let dump_riscv_instr oc (i : ltl_instr) =
  match i with
  | LAddi (rd, rs, i) ->
      Format.fprintf oc "\taddi %s, %s, %d\n" (print_reg rd) (print_reg rs) i
  | LSubi (rd, rs, i) ->
      Format.fprintf oc "\taddi %s, %s, %d\n" (print_reg rd) (print_reg rs) (-i)
  | LBinop (b, rd, rs1, rs2) -> (
      match b with
      | Elang.Eclt ->
          Format.fprintf oc "\tslt %s, %s, %s\n" (print_reg rd) (print_reg rs1)
            (print_reg rs2)
      | Elang.Ecgt ->
          Format.fprintf oc "\tslt %s, %s, %s\n" (print_reg rd) (print_reg rs2)
            (print_reg rs1)
      | Elang.Ecle ->
          (* 'rd <- rs1 <= rs2' == 'rd <- rs2 < rs1; rd <- seqz rd' *)
          Format.fprintf oc "\tslt %s, %s, %s\n" (print_reg rd) (print_reg rs2)
            (print_reg rs1);
          Format.fprintf oc "\tseqz %s, %s\n" (print_reg rd) (print_reg rd)
      | Elang.Ecge ->
          Format.fprintf oc "\tslt %s, %s, %s\n" (print_reg rd) (print_reg rs1)
            (print_reg rs2);
          Format.fprintf oc "\tseqz %s, %s\n" (print_reg rd) (print_reg rd)
      | Elang.Eceq ->
          Format.fprintf oc "\tsub %s, %s, %s\n" (print_reg rd) (print_reg rs1)
            (print_reg rs2);
          Format.fprintf oc "\tseqz %s, %s\n" (print_reg rd) (print_reg rd)
      | Elang.Ecne ->
          Format.fprintf oc "\tsub %s, %s, %s\n" (print_reg rd) (print_reg rs1)
            (print_reg rs2);
          Format.fprintf oc "\tsnez %s, %s\n" (print_reg rd) (print_reg rd)
      | _ ->
          Format.fprintf oc "\t%s %s, %s, %s\n" (print_binop b) (print_reg rd)
            (print_reg rs1) (print_reg rs2))
  | LUnop (u, rd, rs) ->
      Format.fprintf oc "\t%s %s, %s\n" (print_unop u) (print_reg rd)
        (print_reg rs)
  | LStore (rt, i, rs, sz) ->
      let sz = instrsuffix_of_size sz in
      Format.fprintf oc "\ts%c %s, %d(%s)\n" sz (print_reg rs) i (print_reg rt)
  | LLoad (rd, rt, i, sz) ->
      let sz = instrsuffix_of_size sz in
      Format.fprintf oc "\tl%c %s, %d(%s)\n" sz (print_reg rd) i (print_reg rt)
  | LGlobvar (rd, s) -> Format.fprintf oc "\tla %s, %s\n" (print_reg rd) s
  | LMov (rd, rs) ->
      Format.fprintf oc "\tmv %s, %s\n" (print_reg rd) (print_reg rs)
  | LLabel l -> Format.fprintf oc "%s:\n" l
  | LJmp l -> Format.fprintf oc "\tj %s\n" l
  | LJmpr r -> Format.fprintf oc "\tjr %s\n" (print_reg r)
  | LConst (rd, i) -> Format.fprintf oc "\tli %s, %d\n" (print_reg rd) i
  | LComment l -> Format.fprintf oc "# %s\n" l
  | LBranch (cmp, rs1, rs2, s) ->
      Format.fprintf oc "\t%s %s, %s, %s\n" (riscv_of_cmp cmp) (print_reg rs1)
        (print_reg rs2) s
  | LCall fname -> Format.fprintf oc "\tjal ra, %s\n" fname
  | LHalt -> Format.fprintf oc "\thalt\n"

let dump_riscv_fun oc (fname, lf) =
  match lf.ltlfunbody with
  | [] -> ()
  | _ ->
      Format.fprintf oc "%s:\n" fname;
      List.iter (dump_riscv_instr oc) lf.ltlfunbody

let dump_riscv_gvars oc def =
  match def with
  | vname, Gvar (t, i) -> (
      match i with
      | Prog.Iint i ->
          Format.fprintf oc "%s:\n  .byte %s\n" vname
            (split_bytes (Archi.wordsize ()) i
            |> List.map string_of_int |> String.concat ",")
      | Prog.Ichar c ->
          Format.fprintf oc "%s:\n  .byte %s\n" vname
            (split_bytes (Archi.wordsize ()) (Char.code c)
            |> List.map string_of_int |> String.concat ",")
      | Prog.Istring s ->
          Format.fprintf oc "%s:\n  .byte %s\n" vname
            (char_list_of_string s |> List.map Char.code
           |> List.map string_of_int |> String.concat ",")
      | Prog.Ispace n -> Format.fprintf oc "%s:\n  .zero %d\n" vname n)
  | _ -> ()

let riscv_load_args target oc : unit =
  (match target with
  | Linux ->
      [
        LLoad (reg_s1, reg_sp, 0, archi_mas ());
        (* s1 <- argc *)
        LAddi (reg_s2, reg_sp, Archi.wordsize ());
      ]
  | Xv6 -> [ LMov (reg_s1, reg_a0); LMov (reg_s2, reg_a1) ])
  @ [
      LConst (reg_s3, 1);
      LSubi (reg_sp, reg_sp, 72);
      LLabel "Lloop";
      LBranch (Rceq, reg_s3, reg_s1, "Lendargs");
      LMov (reg_a0, reg_t4);
      LAddi (reg_s4, reg_s3, 0);
      LConst (reg_t1, Archi.wordsize ());
      LBinop (Emul, reg_s4, reg_s4, reg_t1);
      LBinop (Eadd, reg_t3, reg_s4, reg_s2);
      LLoad (reg_a0, reg_t3, 0, archi_mas ());
      LCall "atoi";
      LBinop (Esub, reg_s4, reg_fp, reg_s4);
      LStore (reg_s4, 0, reg_a0, archi_mas ());
      LAddi (reg_s3, reg_s3, 1);
      LJmp "Lloop";
      LLabel "Lendargs";
      LLoad (reg_a0, reg_fp, -1 * Archi.wordsize (), archi_mas ());
      LLoad (reg_a1, reg_fp, -2 * Archi.wordsize (), archi_mas ());
      LLoad (reg_a2, reg_fp, -3 * Archi.wordsize (), archi_mas ());
      LLoad (reg_a3, reg_fp, -4 * Archi.wordsize (), archi_mas ());
      LLoad (reg_a4, reg_fp, -5 * Archi.wordsize (), archi_mas ());
      LLoad (reg_a5, reg_fp, -6 * Archi.wordsize (), archi_mas ());
      LLoad (reg_a6, reg_fp, -7 * Archi.wordsize (), archi_mas ());
      LLoad (reg_a7, reg_fp, -8 * Archi.wordsize (), archi_mas ());
    ]
  |> List.iter (dump_riscv_instr oc)

let rv_store () = Format.sprintf "s%c" (Archi.instrsuffix ())
let rv_load () = Format.sprintf "l%c" (Archi.instrsuffix ())

let riscv_prelude target oc =
  Format.fprintf oc ".include \"syscall_numbers.s\"\n";
  Format.fprintf oc ".globl _start\n";
  Format.fprintf oc "_start:\n";
  Format.fprintf oc "\tlui gp, %%hi(_heap_start)\n";
  Format.fprintf oc "\taddi gp, gp, %%lo(_heap_start)\n";
  Format.fprintf oc "\taddi t0, gp, 8\n";
  Format.fprintf oc "\t%s t0, 0(gp)\n" (rv_store ());
  Format.fprintf oc "\tmv s0, sp\n";
  riscv_load_args target oc;
  Format.fprintf oc "\tjal ra, main\n";
  Format.fprintf oc "\tmv s0, a0\n";
  Format.fprintf oc "\tjal ra, println\n";
  Format.fprintf oc "\tmv a0, s0\n";
  Format.fprintf oc "\tjal ra, print_int\n";
  Format.fprintf oc "\tjal ra, println\n";
  Format.fprintf oc "\taddi a7, zero, SYSCALL_EXIT\n";
  Format.fprintf oc "\tecall\n"

let dump_riscv_prog target oc lp : unit =
  if !nostart then () else riscv_prelude target oc;
  Format.fprintf oc "\t.globl main\n";
  Format.fprintf oc "\t.type main, @function\n";
  List.iter
    (function fname, Gfun f -> dump_riscv_fun oc (fname, f) | _ -> ())
    lp;
  Format.fprintf oc ".section .data\n";
  List.iter (dump_riscv_gvars oc) lp
