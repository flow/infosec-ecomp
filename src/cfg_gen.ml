open Batteries
open Elang
open Cfg
open Utils
open Prog
open Report
open Cfg_print
open Options
open Elang_gen

(* [cfg_expr_of_eexpr e] converts an [Elang.expr] into a [expr res]. This should
   always succeed and be straightforward.

   In later versions of this compiler, you will add more things to [Elang.expr]
   but not to [Cfg.expr], hence the distinction.
*)
let rec cfg_expr_of_eexpr typ_fun typ_struct typ_glob func (e : Elang.expr) :
    expr res =
  match e with
  | Elang.Ebinop (b, e1, e2) -> (
      cfg_expr_of_eexpr typ_fun typ_struct typ_glob func e1 >>= fun e1' ->
      cfg_expr_of_eexpr typ_fun typ_struct typ_glob func e2 >>= fun e2' ->
      type_expr func.funvartyp typ_fun typ_struct typ_glob e1 >>= fun t1 ->
      type_expr func.funvartyp typ_fun typ_struct typ_glob e2 >>= fun t2 ->
      match (t1, t2) with
      | (Tptr t | Ttab (t, _)), _ ->
          OK
            (Ebinop (b, e1', Ebinop (Emul, e2', Eint (size_type t typ_struct))))
      | _, Tptr t ->
          OK
            (Ebinop (b, Ebinop (Emul, e1', Eint (size_type t typ_struct)), e2'))
      | _ -> OK (Ebinop (b, e1', e2')))
  | Elang.Eunop (u, e) ->
      cfg_expr_of_eexpr typ_fun typ_struct typ_glob func e >>= fun e' ->
      OK (Eunop (u, e'))
  | Elang.Eint i -> OK (Eint i)
  | Elang.Evar v -> (
      match Hashtbl.find_option func.funvarinmem v with
      | Some offset -> (
          type_expr func.funvartyp typ_fun typ_struct typ_glob e >>= fun t ->
          match t with
          | Tint | Tchar ->
              size_type (Hashtbl.find func.funvartyp v) typ_struct
              |> fun size -> OK (Eload (Estk offset, size))
          | Tstruct _ | Ttab _ | Tptr _ -> OK (Estk offset)
          | Tvoid -> Error "E: Cannot use void variable.\n")
      | None -> OK (Evar v))
  | Elang.Eglobvar x -> (
      Hashtbl.find_option typ_glob x ||> "E: Global variable not found.\n"
      >>= fun t ->
      match t with
      | Tint | Tchar | Tptr _ -> OK (Eload (Eglobvar x, size_type t typ_struct))
      | Tstruct _ | Ttab _ | Tvoid -> OK (Eglobvar x))
  | Elang.Echar c -> OK (Eint (int_of_char c))
  | Elang.Ecall (s, exprs) ->
      list_map_res (cfg_expr_of_eexpr typ_fun typ_struct typ_glob func) exprs
      >>= fun exprs -> OK (Ecall (s, exprs))
  | Elang.Eaddrof a -> (
      match a with
      | Elang.Evar v ->
          Hashtbl.find_option func.funvarinmem v
          ||> Format.sprintf "E: Variable %s not found.\n" v
          >>= fun offset -> OK (Estk offset)
      | Elang.Eglobvar x -> OK (Eglobvar x)
      | Elang.Eload e ->
          cfg_expr_of_eexpr typ_fun typ_struct typ_glob func e >>= fun e' ->
          OK e'
      | _ -> Error "E: Only variables can be taken the address of.\n")
  | Elang.Eload e -> (
      cfg_expr_of_eexpr typ_fun typ_struct typ_glob func e >>= fun e' ->
      type_expr func.funvartyp typ_fun typ_struct typ_glob e >>= fun t ->
      match t with
      | Tptr t -> (
          match t with
          | Tint | Tchar | Tptr _ ->
              size_type t typ_struct |> fun size -> OK (Eload (e', size))
          | Tstruct _ | Ttab _ | Tvoid -> OK e')
      | _ -> Error "Elang.eload: Only pointers can be dereferenced.\n")
  | Elang.Egetfield (e, f) -> (
      cfg_expr_of_eexpr typ_fun typ_struct typ_glob func e >>= fun e' ->
      type_expr func.funvartyp typ_fun typ_struct typ_glob e >>= fun t ->
      match t with
      | Tstruct sname -> (
          field_offset typ_struct sname f >>= fun offset ->
          field_type typ_struct sname f >>= fun t ->
          match t with
          | Tint | Tchar | Tptr _ ->
              size_type t typ_struct |> fun size ->
              OK (Eload (Ebinop (Eadd, e', Eint offset), size))
          | Tstruct _ | Ttab _ | Tvoid -> OK (Ebinop (Eadd, e', Eint offset)))
      | _ -> Error "Elang.egetfield: Only pointers to structs can be indexed.\n"
      )

(* [cfg_node_of_einstr next cfg succ i] builds the CFG node(s) that correspond
   to the E instruction [i].

   [cfg] is the current state of the control-flow graph.

   [succ] is the successor of this node in the CFG, i.e. where to go after this
   instruction.

   [next] is the next available CFG node identifier.

   This function returns a pair (n, next) where [n] is the identifer of the
   node generated, and [next] is the new next available CFG node identifier.

   Hint: several nodes may be generated for a single E instruction.
*)
let rec cfg_node_of_einstr typ_fun typ_struct typ_glob func (next : int)
    (cfg : (int, cfg_node) Hashtbl.t) (succ : int) (i : instr) : (int * int) res
    =
  match i with
  | Elang.Idecl _ -> OK (succ, next)
  | Elang.Ideclinit (_, v, e) | Elang.Iassign (v, e) -> (
      cfg_expr_of_eexpr typ_fun typ_struct typ_glob func e >>= fun e ->
      if is_glob_var func.funvartyp typ_glob v then (
        size_type (Hashtbl.find typ_glob v) typ_struct |> fun size ->
        Hashtbl.replace cfg next (Cstore (Eglobvar v, e, size, succ));
        OK (next, next + 1))
      else
        match Hashtbl.find_option func.funvarinmem v with
        | Some offset ->
            size_type (Hashtbl.find func.funvartyp v) typ_struct |> fun size ->
            Hashtbl.replace cfg next (Cstore (Estk offset, e, size, succ));
            OK (next, next + 1)
        | None ->
            Hashtbl.replace cfg next (Cassign (v, e, succ));
            OK (next, next + 1))
  | Elang.Iif (c, ithen, ielse) ->
      cfg_expr_of_eexpr typ_fun typ_struct typ_glob func c >>= fun c ->
      cfg_node_of_einstr typ_fun typ_struct typ_glob func next cfg succ ithen
      >>= fun (nthen, next) ->
      cfg_node_of_einstr typ_fun typ_struct typ_glob func next cfg succ ielse
      >>= fun (nelse, next) ->
      Hashtbl.replace cfg next (Ccmp (c, nthen, nelse));
      OK (next, next + 1)
  | Elang.Iwhile (c, i) ->
      cfg_expr_of_eexpr typ_fun typ_struct typ_glob func c >>= fun c ->
      let cmp, next = (next, next + 1) in
      cfg_node_of_einstr typ_fun typ_struct typ_glob func next cfg cmp i
      >>= fun (nthen, next) ->
      Hashtbl.replace cfg cmp (Ccmp (c, nthen, succ));
      OK (cmp, next + 1)
  | Elang.Iblock il ->
      List.fold_right
        (fun i acc ->
          acc >>= fun (succ, next) ->
          cfg_node_of_einstr typ_fun typ_struct typ_glob func next cfg succ i)
        il
        (OK (succ, next))
  | Elang.Ireturn e ->
      cfg_expr_of_eexpr typ_fun typ_struct typ_glob func e >>= fun e ->
      Hashtbl.replace cfg next (Creturn e);
      OK (next, next + 1)
  | Elang.Icall (s, exprs) ->
      list_map_res (cfg_expr_of_eexpr typ_fun typ_struct typ_glob func) exprs
      >>= fun exprs ->
      Hashtbl.replace cfg next (Ccall (s, exprs, succ));
      OK (next, next + 1)
  | Elang.Istore (a, v) -> (
      cfg_expr_of_eexpr typ_fun typ_struct typ_glob func a >>= fun e' ->
      cfg_expr_of_eexpr typ_fun typ_struct typ_glob func v >>= fun v ->
      type_expr func.funvartyp typ_fun typ_struct typ_glob a >>= fun t ->
      match t with
      | Tptr t ->
          size_type t typ_struct |> fun size ->
          Hashtbl.replace cfg next (Cstore (e', v, size, succ));
          OK (next, next + 1)
      | _ -> Error "Elang.istore: Only pointers can be dereferenced.\n")
  | Elang.Isetfield (e, f, v) -> (
      cfg_expr_of_eexpr typ_fun typ_struct typ_glob func e >>= fun e' ->
      cfg_expr_of_eexpr typ_fun typ_struct typ_glob func v >>= fun v ->
      type_expr func.funvartyp typ_fun typ_struct typ_glob e >>= fun t ->
      match t with
      | Tstruct sname ->
          field_offset typ_struct sname f >>= fun offset ->
          field_type typ_struct sname f >>= fun t ->
          size_type t typ_struct |> fun size ->
          Hashtbl.replace cfg next
            (Cstore (Ebinop (Eadd, e', Eint offset), v, size, succ));
          OK (next, next + 1)
      | _ -> Error "Elang.egetfield: Only pointers to structs can be indexed.\n"
      )

(* Some nodes may be unreachable after the CFG is entirely generated. The
   [reachable_nodes n cfg] constructs the set of node identifiers that are
   reachable from the entry node [n]. *)
let rec reachable_nodes n (cfg : (int, cfg_node) Hashtbl.t) =
  let rec reachable_aux n reach =
    if Set.mem n reach then reach
    else
      let reach = Set.add n reach in
      match Hashtbl.find_option cfg n with
      | None | Some (Creturn _) -> reach
      | Some (Cnop succ)
      | Some (Cassign (_, _, succ))
      | Some (Ccall (_, _, succ))
      | Some (Cstore (_, _, _, succ)) ->
          reachable_aux succ reach
      | Some (Ccmp (_, s1, s2)) -> reachable_aux s1 (reachable_aux s2 reach)
  in
  reachable_aux n Set.empty

(* [cfg_fun_of_efun f] builds the CFG for E function [f]. *)
let cfg_fun_of_efun typ_fun typ_struct typ_glob func =
  let cfg = Hashtbl.create 17 in
  Hashtbl.replace cfg 0 (Creturn (Eint 0));
  cfg_node_of_einstr typ_fun typ_struct typ_glob func 1 cfg 0 func.funbody
  >>= fun (node, _) ->
  (* remove unreachable nodes *)
  let r = reachable_nodes node cfg in
  Hashtbl.filteri_inplace (fun k _ -> Set.mem k r) cfg;
  OK
    {
      cfgfunargs = List.map fst func.funargs;
      cfgfunbody = cfg;
      cfgentry = node;
      cfgfunstksz = func.funstksz;
    }

let cfg_gdef_of_edef typ_fun typ_struct typ_glob gd =
  match gd with
  | Gfun f ->
      cfg_fun_of_efun typ_fun typ_struct typ_glob f >>= fun f -> OK (Gfun f)
  | Gvar (t, v) -> OK (Gvar (t, v))

let cfg_prog_of_eprog typ_struct efuns : cfg_fun prog res =
  let typ_fun = Hashtbl.create (List.length efuns + 3) in
  let typ_glob = Hashtbl.create 17 in
  Hashtbl.replace typ_fun "print" ([ Tint ], Tvoid);
  Hashtbl.replace typ_fun "print_int" ([ Tint ], Tvoid);
  Hashtbl.replace typ_fun "print_char" ([ Tchar ], Tvoid);
  List.iter
    (fun (name, def) ->
      match def with
      | Gvar (t, _) -> Hashtbl.replace typ_glob name t
      | Gfun func ->
          Hashtbl.add typ_fun name
            (snd (List.split func.funargs), func.funrettyp))
    efuns;
  assoc_map_res (fun _ -> cfg_gdef_of_edef typ_fun typ_struct typ_glob) efuns

let pass_cfg_gen ep =
  let efuns, typ_struct = ep in
  cfg_prog_of_eprog typ_struct efuns |> function
  | Error msg ->
      record_compile_result ~error:(Some msg) "CFG";
      Error msg
  | OK cfg ->
      record_compile_result "CFG";
      dump !cfg_dump dump_cfg_prog cfg (call_dot "cfg" "CFG");
      OK cfg
