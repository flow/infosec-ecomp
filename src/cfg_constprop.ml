open Batteries
open Cfg
open Elang_run
open Prog
open Utils
open Report
open Cfg_print
open Options

(* [simple_eval_eexpr e] evaluates an expression [e] with no variables. Raises
   an exception if the expression contains variables. *)
let rec simple_eval_eexpr (e : expr) : int =
  match e with
  | Ebinop (b, e1, e2) ->
      eval_binop b (simple_eval_eexpr e1) (simple_eval_eexpr e2)
  | Eunop (u, e) -> eval_unop u (simple_eval_eexpr e)
  | Eint i -> i
  | Evar _ -> raise (Failure "simple_eval_eexpr: expression has variable")
  | Eglobvar _ ->
      raise (Failure "simple_eval_eexpr: expression has global variable")
  | Ecall _ -> raise (Failure "simple_eval_eexpr: expression has call")
  | Estk _ -> raise (Failure "simple_eval_eexpr: expression has stack")
  | Eload _ -> raise (Failure "simple_eval_eexpr: expression has load")

(* If an expression contains variables, we cannot simply evaluate it. *)

(* [has_vars e] indicates whether [e] contains variables. *)
let rec has_vars (e : expr) =
  match e with
  | Ebinop (_, e1, e2) -> has_vars e1 || has_vars e2
  | Eunop (_, e) -> has_vars e
  | Eint _ -> false
  | Evar _ | Eglobvar _ | Ecall _ | Estk _ | Eload _ -> true

let const_prop_binop b e1 e2 =
  let e = Ebinop (b, e1, e2) in
  if has_vars e then e else Eint (simple_eval_eexpr e)

let const_prop_unop u e =
  let e = Eunop (u, e) in
  if has_vars e then e else Eint (simple_eval_eexpr e)

let rec const_prop_expr (e : expr) =
  if has_vars e then e
  else
    match e with
    | Ebinop (b, e1, e2) ->
        const_prop_binop b (const_prop_expr e1) (const_prop_expr e2)
    | Eunop (u, e) -> const_prop_unop u (const_prop_expr e)
    | Eint _ | Evar _ | Eglobvar _ | Ecall _ | Estk _ | Eload _ -> e

let constant_propagation_instr (i : cfg_node) : cfg_node =
  match i with
  | Cassign (x, e, s) -> Cassign (x, const_prop_expr e, s)
  | Creturn e -> Creturn (const_prop_expr e)
  | Ccmp (e, s1, s2) -> Ccmp (const_prop_expr e, s1, s2)
  | Cnop _ -> i
  | Ccall _ -> i
  | Cstore _ -> i

let constant_propagation_fun ({ cfgfunbody; _ } as f : cfg_fun) =
  let ht = Hashtbl.map (fun _ m -> constant_propagation_instr m) cfgfunbody in
  { f with cfgfunbody = ht }

let constant_propagation_gdef = function
  | Gfun f -> Gfun (constant_propagation_fun f)
  | g -> g

let constant_propagation p =
  if !Options.no_cfg_constprop then p else assoc_map constant_propagation_gdef p

let pass_constant_propagation p =
  let cfg = constant_propagation p in
  record_compile_result "Constprop";
  dump
    (!cfg_dump >*> fun s -> s ^ "1")
    dump_cfg_prog cfg
    (call_dot "cfg-after-cstprop" "CFG after Constant Propagation");
  OK cfg
