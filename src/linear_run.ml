open Batteries
open BatList
open Prog
open Elang
open Cfg
open Elang_run
open Cfg_run
open Rtl
open Rtl_print
open Rtl_run
open Linear
open Builtins
open Utils

let rec exec_linear_instr oc lp fname f sp st (i : rtl_instr) =
  match i with
  | Rbinop (b, rd, rs1, rs2) -> (
      match
        (Hashtbl.find_option st.regs rs1, Hashtbl.find_option st.regs rs2)
      with
      | Some v1, Some v2 ->
          Hashtbl.replace st.regs rd (eval_binop b v1 v2);
          OK (None, st)
      | _, _ ->
          Error
            (Printf.sprintf "Binop applied on undefined registers (%s and %s)"
               (print_reg rs1) (print_reg rs2)))
  | Runop (u, rd, rs) -> (
      match Hashtbl.find_option st.regs rs with
      | Some v ->
          Hashtbl.replace st.regs rd (eval_unop u v);
          OK (None, st)
      | _ ->
          Error
            (Printf.sprintf "Unop applied on undefined register %s"
               (print_reg rs)))
  | Rconst (rd, i) ->
      Hashtbl.replace st.regs rd i;
      OK (None, st)
  | Rbranch (cmp, r1, r2, s1) -> (
      match
        (Hashtbl.find_option st.regs r1, Hashtbl.find_option st.regs r2)
      with
      | Some v1, Some v2 ->
          if eval_rtl_cmp cmp v1 v2 then
            exec_linear_instr_at oc lp fname f sp st s1
          else OK (None, st)
      | _, _ ->
          Error
            (Printf.sprintf "Branching on undefined registers (%s and %s)"
               (print_reg r1) (print_reg r2)))
  | Rjmp s -> exec_linear_instr_at oc lp fname f sp st s
  | Rmov (rd, rs) -> (
      match Hashtbl.find_option st.regs rs with
      | Some s ->
          Hashtbl.replace st.regs rd s;
          OK (None, st)
      | _ ->
          Error (Printf.sprintf "Mov on undefined register (%s)" (print_reg rs))
      )
  | Rret r -> (
      match Hashtbl.find_option st.regs r with
      | Some s -> OK (Some s, st)
      | _ ->
          Error (Printf.sprintf "Ret on undefined register (%s)" (print_reg r)))
  | Rlabel n -> OK (None, st)
  | Rcall (rd, fname, args) -> (
      List.map (Hashtbl.find st.regs) args |> fun params ->
      match find_function lp fname with
      | Error e -> do_builtin oc st.mem fname params >>= fun ret -> OK (ret, st)
      | OK f -> (
          exec_linear_fun oc lp (sp + f.linearfunstksz) st fname f params
          >>= fun (ret, st) ->
          match rd with
          | None -> OK (None, st)
          | Some rd -> (
              match ret with
              | None ->
                  Error
                    (Printf.sprintf
                       "Function %s returned void, but %s is not void" fname
                       (print_reg rd))
              | Some ret ->
                  Hashtbl.replace st.regs rd ret;
                  OK (None, st))))
  | Rstk (rd, addr) ->
      Hashtbl.replace st.regs rd (sp + addr);
      OK (None, st)
  | Rload (rd, rs, size) ->
      Hashtbl.find st.regs rs |> fun addr ->
      Mem.read_bytes_as_int st.mem addr size >>= fun value ->
      Hashtbl.replace st.regs rd value;
      OK (None, st)
  | Rglobvar (rd, name) ->
      Hashtbl.replace st.regs rd (Hashtbl.find st.glob_env name);
      OK (None, st)
  | Rstore (rd, rs, size) ->
      Hashtbl.find st.regs rs |> fun value ->
      Hashtbl.find st.regs rd |> fun addr ->
      split_bytes size value |> fun bytes ->
      Mem.write_bytes st.mem addr bytes >>= fun _ -> OK (None, st)

and exec_linear_instr_at oc lp fname ({ linearfunbody } as f) sp st i =
  let l = List.drop_while (fun x -> x <> Rlabel i) linearfunbody in
  exec_linear_instrs oc lp fname f sp st l

and exec_linear_instrs oc lp fname f sp st l =
  List.fold_left
    (fun acc i ->
      match acc with
      | Error _ -> acc
      | OK (Some v, st) -> OK (Some v, st)
      | OK (None, st) -> exec_linear_instr oc lp fname f sp st i)
    (OK (None, st))
    l

and exec_linear_fun oc lp sp st fname f params =
  let regs' = Hashtbl.create 17 in
  match
    List.iter2 (fun n v -> Hashtbl.replace regs' n v) f.linearfunargs params
  with
  | exception Invalid_argument _ ->
      Error
        (Format.sprintf
           "Linear: Called function %s with %d arguments, expected %d\n" fname
           (List.length params)
           (List.length f.linearfunargs))
  | _ ->
      let l = f.linearfunbody in
      let regs_save = Hashtbl.copy st.regs in
      let st' = { st with regs = regs' } in
      exec_linear_instrs oc lp fname f sp st' l >>= fun (v, st) ->
      OK (v, { st with regs = regs_save })

and exec_linear_prog oc lp memsize params =
  let st = init_state memsize in
  find_function lp "main" >>= fun f ->
  let n = List.length f.linearfunargs in
  let params = take n params in
  List.fold
    (fun startglob (name, def) ->
      match def with
      | Gvar (t, s) ->
          startglob >>= fun startglob ->
          init_glob st.mem st.glob_env [ (name, def) ] startglob
      | Gfun _ -> startglob)
    (OK 0x1000) lp
  >>= fun _ ->
  exec_linear_fun oc lp 0 st "main" f params >>= fun (v, st) -> OK v
