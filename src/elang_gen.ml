open Ast
open Elang
open Prog
open Report
open Options
open Batteries
open Elang_print
open Utils

let tag_is_binop = function
  | Tadd -> true
  | Tsub -> true
  | Tmul -> true
  | Tdiv -> true
  | Tmod -> true
  | Txor -> true
  | Tcle -> true
  | Tclt -> true
  | Tcge -> true
  | Tcgt -> true
  | Tceq -> true
  | Tne -> true
  | Tand -> true
  | Tor -> true
  | Tband -> true
  | _ -> false

let tag_is_unop = function Tneg -> true | Tnot -> true | _ -> false

let rec tree_is_typ : tree -> bool = function
  | Node ((Tint | Tchar | Tvoid), []) -> true
  | Node (Tpointer, [ t ]) -> tree_is_typ t
  | Node (Tstruct, [ StringLeaf _ ]) -> true
  | Node (Tarray, [ t; IntLeaf _ ]) -> tree_is_typ t
  | _ -> false

let rec typ_of_tree (t : tree) : typ =
  match t with
  | Node (Tint, []) -> Tint
  | Node (Tchar, []) -> Tchar
  | Node (Tvoid, []) -> Tvoid
  | Node (Tpointer, [ t ]) -> Tptr (typ_of_tree t)
  | Node (Tstruct, [ StringLeaf s ]) -> Tstruct s
  | Node (Tarray, [ t; IntLeaf n ]) -> Ttab (typ_of_tree t, n)
  | _ -> failwith "typ_of_tag: not a type"

let binop_of_tag = function
  | Tadd -> Eadd
  | Tsub -> Esub
  | Tmul -> Emul
  | Tdiv -> Ediv
  | Tmod -> Emod
  | Txor -> Exor
  | Tcle -> Ecle
  | Tclt -> Eclt
  | Tcge -> Ecge
  | Tcgt -> Ecgt
  | Tceq -> Eceq
  | Tne -> Ecne
  | Tand -> Eand
  | Tor -> Eor
  | Tband -> Eband
  | _ -> assert false

let unop_of_tag = function Tneg -> Eneg | Tnot -> Enot | _ -> assert false

(* [get_op_typ op t1 t2] returns OK resTyp if  the operation is valid between t1 an t2
    and returns Error msg if the operation is not valid between t1 and t2 *)
let get_op_typ (op : binop) t1 t2 : typ res =
  match op with
  (* op can be a comparison operation *)
  | Ecle | Eclt | Ecge | Ecgt | Eceq | Ecne -> (
      match (t1, t2) with
      | Tptr t, Tptr t' when t = t' -> OK Tint
      | Tptr _, _ | _, Tptr _ -> Error "Cannot compare pointers"
      | Tvoid, _ | _, Tvoid -> Error "Cannot compare void types"
      | _ -> OK Tint)
  (* op can be a (+) *)
  | Eadd -> (
      match (t1, t2) with
      | Tptr _, Tptr _ -> Error "Cannot add two pointers"
      | Tvoid, _ | _, Tvoid -> Error "Cannot add void types"
      | Tptr t, _ | _, Tptr t -> OK (Tptr t)
      | Ttab (t, _), (Tint | Tchar) -> OK (Tptr t)
      | _ -> OK Tint)
  (* op can be a (-) *)
  | Esub -> (
      match (t1, t2) with
      | _, Tptr _ -> Error "Cannot substract a pointer"
      | Tptr _, (Tint | Tchar) -> OK t1
      | Tvoid, _ | _, Tvoid -> Error "Cannot substract void types"
      | _ -> OK Tint)
  (* op can be multiplicative: (/, *, %) *)
  | Emul | Ediv | Emod -> (
      match (t1, t2) with
      | Tptr _, _ | _, Tptr _ -> Error "Cannot multiply or divide a pointer"
      | Tvoid, _ | _, Tvoid -> Error "Cannot multiply or divide void types"
      | _ -> OK Tint)
  (* op can be a bitwise operation: (^) *)
  | Exor | Eband -> (
      match (t1, t2) with
      | Tptr _, _ | _, Tptr _ ->
          Error "Cannot perform a bitwise operation on a pointer"
      | Tvoid, _ | _, Tvoid ->
          Error "Cannot perform a bitwise operation on void types"
      | _ -> OK Tint)
  (* op can be a logical operation: (&&, ||) *)
  | Eand | Eor -> (
      match (t1, t2) with
      | Tptr _, _ | _, Tptr _ ->
          Error "Cannot perform a logical operation on a pointer"
      | Tvoid, _ | _, Tvoid ->
          Error "Cannot perform a logical operation on void types"
      | _ -> OK Tint)

(* [is_glob_var typ_var typ_glob s] returns wether the var is global or local*)
let is_glob_var typ_var typ_glob s =
  Hashtbl.mem typ_glob s && not (Hashtbl.mem typ_var s)

let type_var typ_var typ_glob s =
  if is_glob_var typ_var typ_glob s then Hashtbl.find typ_glob s
  else Hashtbl.find typ_var s

(* [type_expr typ_var typ_fun e] returns the type of an expression [e] in a
    context where the type of variables is given by [typ_var] and the type of
    functions is given by [typ_fun]. *)
let rec type_expr typ_var typ_fun typ_struct typ_glob (e : expr) : typ res =
  match e with
  | Evar x ->
      Hashtbl.find_option typ_var x
      ||> Printf.sprintf "type_expr: variable %s not defined" x
  | Eglobvar x ->
      Hashtbl.find_option typ_glob x
      ||> Printf.sprintf "type_expr: global variable %s not defined" x
  | Eint _ -> OK Tint
  | Echar _ -> OK Tchar
  | Ebinop (op, e1, e2) -> (
      type_expr typ_var typ_fun typ_struct typ_glob e1 >>= fun t1 ->
      type_expr typ_var typ_fun typ_struct typ_glob e2 >>= fun t2 ->
      match get_op_typ op t1 t2 with
      | OK t -> OK t
      | Error _ ->
          Error
            (Printf.sprintf
               "Type error in binary operation uncompatible types: %s %s %s"
               (string_of_typ t1) (dump_binop op) (string_of_typ t2)))
  | Eunop (op, e) -> (
      type_expr typ_var typ_fun typ_struct typ_glob e >>= fun t ->
      match op with
      | Eneg | Enot ->
          if t === Tint then OK Tint
          else Error "type_expr: type error in unary operation")
  | Ecall (fname, args) ->
      Hashtbl.find_option typ_fun fname ||> "type_expr: function not defined"
      >>= fun (arg_types, ret_type) ->
      list_map_res (type_expr typ_var typ_fun typ_struct typ_glob) args
      >>= fun arg_types' ->
      if
        List.for_all2 ( === ) arg_types
          (List.take (List.length arg_types) arg_types')
      then OK ret_type
      else Error "type_expr: type error in function call"
  | Eaddrof e ->
      type_expr typ_var typ_fun typ_struct typ_glob e >>= fun t -> OK (Tptr t)
  | Eload e -> (
      type_expr typ_var typ_fun typ_struct typ_glob e >>= fun t ->
      match t with
      | Tptr t -> OK t
      | Ttab (t, _) -> OK t
      | Tstruct s -> OK t
      | _ ->
          Error
            (Printf.sprintf "type_expr: type error in load: %s\n"
               (string_of_typ t)))
  | Egetfield (e, f) -> (
      type_expr typ_var typ_fun typ_struct typ_glob e >>= fun t ->
      match t with
      | Tstruct s -> field_type typ_struct s f
      | _ -> Error "type_expr: type error in getfield")

(* [make_eexpr_of_ast a] builds an expression corresponding to a tree [a]. If
   the tree is not well-formed, fails with an [Error] message. *)
let rec make_eexpr_of_ast typ_var typ_fun typ_struct typ_glob strings (a : tree)
    : expr res =
  let res =
    match a with
    | StringLeaf s ->
        if is_glob_var typ_var typ_glob s then OK (Eglobvar s) else OK (Evar s)
    | StringLiteral s ->
        let var = Printf.sprintf "str_%d" (Hashtbl.length typ_glob) in
        Hashtbl.add typ_glob var (Ttab (Tchar, String.length s + 1));
        Hashtbl.add strings var (s ^ "\000");
        OK (Eglobvar var)
    | IntLeaf i -> OK (Eint i)
    | NullLeaf -> OK (Eint 0)
    | CharLeaf c -> OK (Echar c)
    | Node (Tmember, [ e; StringLeaf f ]) -> (
        make_eexpr_of_ast typ_var typ_fun typ_struct typ_glob strings e
        >>= fun e' ->
        type_expr typ_var typ_fun typ_struct typ_glob e' >>= fun t ->
        match t with
        | Tstruct n ->
            field_type typ_struct n f >>= fun t -> OK (Egetfield (e', f))
        | _ ->
            Error
              (Printf.sprintf
                 "make_eexpr_of_ast: type error in member expected struct got %s\n"
                 (string_of_typ t)))
    | Node (Tmemberptr, [ e; StringLeaf f ]) -> (
        make_eexpr_of_ast typ_var typ_fun typ_struct typ_glob strings e
        >>= fun e' ->
        type_expr typ_var typ_fun typ_struct typ_glob e' >>= fun t ->
        match t with
        | Tptr (Tstruct n) ->
            field_type typ_struct n f >>= fun t -> OK (Egetfield (Eload e', f))
        | _ ->
            Error
              (Printf.sprintf
                 "make_eexpr_of_ast: type error in member expected struct got %s\n"
                 (string_of_typ t)))
    | Node (op, [ e1; e2 ]) when tag_is_binop op -> (
        make_eexpr_of_ast typ_var typ_fun typ_struct typ_glob strings e1
        >>= fun e1' ->
        make_eexpr_of_ast typ_var typ_fun typ_struct typ_glob strings e2
        >>= fun e2' ->
        type_expr typ_var typ_fun typ_struct typ_glob e1' >>= fun t1 ->
        type_expr typ_var typ_fun typ_struct typ_glob e2' >>= fun t2 ->
        binop_of_tag op |> fun op ->
        match get_op_typ op t1 t2 with
        | OK _ -> OK (Ebinop (op, e1', e2'))
        | Error msg ->
            Error
              (Printf.sprintf
                 "Type error in binary operation uncompatible types: %s %s %s"
                 (string_of_typ t1) (dump_binop op) (string_of_typ t2)))
    | Node (op, [ e ]) when tag_is_unop op ->
        unop_of_tag op |> fun op ->
        make_eexpr_of_ast typ_var typ_fun typ_struct typ_glob strings e
        >>= fun e' ->
        type_expr typ_var typ_fun typ_struct typ_glob e' >>= fun t ->
        if t === Tint then OK (Eunop (op, e'))
        else
          Error
            (Printf.sprintf "Type error in unary operation %s %s" (dump_unop op)
               (string_of_ast e))
    | Node (Tfuncall, [ StringLeaf fname; Node (Tfuncallargs, args) ]) ->
        Hashtbl.find_option typ_fun fname
        ||> Printf.sprintf "Function %s is not defined" fname
        >>= fun (argtypedef, rettype) ->
        list_map_res
          (make_eexpr_of_ast typ_var typ_fun typ_struct typ_glob strings)
          args
        >>= fun args ->
        list_map_res (type_expr typ_var typ_fun typ_struct typ_glob) args
        >>= fun arg_types ->
        if List.length arg_types <> List.length argtypedef then
          Error
            (Printf.sprintf
               "Type error wrong number of args in function call `%s` expected \
                (%s) got (%s)"
               fname
               (string_of_typ_list argtypedef)
               (string_of_typ_list arg_types))
        else if List.exists2 ( =/= ) arg_types argtypedef then
          Error
            (Printf.sprintf "Type error in function call %s expected %s got %s"
               fname
               (string_of_typ_list argtypedef)
               (string_of_typ_list arg_types))
        else OK (Ecall (fname, args))
    | Node (Tref, [ e ]) ->
        make_eexpr_of_ast typ_var typ_fun typ_struct typ_glob strings e
        >>= fun e' -> OK (Eaddrof e')
    | Node (Tvalue, [ e ]) ->
        make_eexpr_of_ast typ_var typ_fun typ_struct typ_glob strings e
        >>= fun e' ->
        type_expr typ_var typ_fun typ_struct typ_glob e' >>= fun t ->
        if is_ptr t then OK (Eload e')
        else
          Error
            (Printf.sprintf "Type error in dereference %s %s"
               (string_of_tag Tvalue) (string_of_ast e))
    | Node (Tindex, [ e1; e2 ]) -> (
        make_eexpr_of_ast typ_var typ_fun typ_struct typ_glob strings e1
        >>= fun e1' ->
        make_eexpr_of_ast typ_var typ_fun typ_struct typ_glob strings e2
        >>= fun e2' ->
        type_expr typ_var typ_fun typ_struct typ_glob e1' >>= fun t1 ->
        type_expr typ_var typ_fun typ_struct typ_glob e2' >>= fun t2 ->
        match (t1, t2) with
        | (Ttab _ | Tptr _), _ when t2 === Tint ->
            OK (Eload (Ebinop (Eadd, e1', e2')))
        | _ ->
            Error
              (Printf.sprintf "Type error in index %s %s" (string_of_tag Tindex)
                 (string_of_ast a)))
    | _ ->
        Error
          (Printf.sprintf "Unacceptable ast in make_eexpr_of_ast %s"
             (string_of_ast a))
  in
  res

let rec make_einstr_of_ast typ_var typ_fun typ_struct typ_glob strings
    (funrettyp : typ) (a : tree) : instr res =
  let res =
    match a with
    | Node (Tif, cond :: iftrue :: [ iffalse ]) ->
        make_eexpr_of_ast typ_var typ_fun typ_struct typ_glob strings cond
        >>= fun cond ->
        make_einstr_of_ast typ_var typ_fun typ_struct typ_glob strings funrettyp
          iftrue
        >>= fun iftrue ->
        make_einstr_of_ast typ_var typ_fun typ_struct typ_glob strings funrettyp
          iffalse
        >>= fun iffalse -> OK (Iif (cond, iftrue, iffalse))
    | Node (Tif, [ cond; iftrue ]) ->
        make_eexpr_of_ast typ_var typ_fun typ_struct typ_glob strings cond
        >>= fun cond ->
        make_einstr_of_ast typ_var typ_fun typ_struct typ_glob strings funrettyp
          iftrue
        >>= fun iftrue -> OK (Iif (cond, iftrue, Iblock []))
    | Node (Treturn, [ e ]) ->
        make_eexpr_of_ast typ_var typ_fun typ_struct typ_glob strings e
        >>= fun e' ->
        type_expr typ_var typ_fun typ_struct typ_glob e' >>= fun t ->
        if t === funrettyp then OK (Ireturn e')
        else
          Error
            (Printf.sprintf "Type error in return: %s is %s but should be %s"
               (dump_eexpr e') (string_of_typ t) (string_of_typ funrettyp))
    | Node (Twhile, [ cond; body ]) ->
        make_eexpr_of_ast typ_var typ_fun typ_struct typ_glob strings cond
        >>= fun cond ->
        make_einstr_of_ast typ_var typ_fun typ_struct typ_glob strings funrettyp
          body
        >>= fun body -> OK (Iwhile (cond, body))
    | Node (Tassign, [ Node (t, ts) ]) -> (
        match (t, ts) with
        | Tassignvar, [ StringLeaf id; e ] -> (
            make_eexpr_of_ast typ_var typ_fun typ_struct typ_glob strings e
            >>= fun e' ->
            type_expr typ_var typ_fun typ_struct typ_glob e' >>= fun t ->
            match
              (Hashtbl.find_option typ_var id, Hashtbl.find_option typ_glob id)
            with
            | (Some t', _ | None, Some t') when t === t' ->
                OK (Iassign (id, e'))
            | Some t', _ | _, Some t' ->
                Error
                  (Printf.sprintf
                     "Type error in assignment %s: %s is %s but should be %s" id
                     (dump_eexpr e') (string_of_typ t') (string_of_typ t))
            | None, None ->
                Error
                  (Printf.sprintf
                     "Variable %s is not defined in assignment %s = %s" id id
                     (dump_eexpr e')))
        | Tassignptr, [ e; v ] ->
            make_eexpr_of_ast typ_var typ_fun typ_struct typ_glob strings e
            >>= fun e' ->
            make_eexpr_of_ast typ_var typ_fun typ_struct typ_glob strings v
            >>= fun v' ->
            type_expr typ_var typ_fun typ_struct typ_glob e' >>= fun t ->
            type_expr typ_var typ_fun typ_struct typ_glob v' >>= fun t' ->
            if t = Tptr t' then OK (Istore (e', v'))
            else
              Error
                (Printf.sprintf
                   "Type error in assignment %s = %s: %s is %s but should be %s"
                   (dump_eexpr e') (dump_eexpr v') (dump_eexpr e')
                   (string_of_typ t) (string_of_typ Tint))
        | ((Tassignmember | Tassignmemberptr) as tag), [ e; StringLeaf f; v ]
          -> (
            make_eexpr_of_ast typ_var typ_fun typ_struct typ_glob strings e
            >>= fun e' ->
            type_expr typ_var typ_fun typ_struct typ_glob e' >>= fun t ->
            match (tag, t) with
            | Tassignmember, Tstruct name ->
                field_type typ_struct name f >>= fun t ->
                make_eexpr_of_ast typ_var typ_fun typ_struct typ_glob strings v
                >>= fun v' ->
                type_expr typ_var typ_fun typ_struct typ_glob v' >>= fun t' ->
                if t === t' then OK (Isetfield (e', f, v'))
                else Error "Type error in assignment"
            | Tassignmemberptr, Tptr (Tstruct name) ->
                field_type typ_struct name f >>= fun t ->
                make_eexpr_of_ast typ_var typ_fun typ_struct typ_glob strings v
                >>= fun v' ->
                type_expr typ_var typ_fun typ_struct typ_glob v' >>= fun t' ->
                if t === t' then OK (Isetfield (Eload e', f, v'))
                else Error "Type error in assignment"
            | _ ->
                Error
                  (Printf.sprintf
                     "Type error in assignment expected struct or \
                      structpointer got %s\n"
                     (string_of_typ t)))
        | Tassignindex, [ e; i; v ] -> (
            make_eexpr_of_ast typ_var typ_fun typ_struct typ_glob strings e
            >>= fun e' ->
            make_eexpr_of_ast typ_var typ_fun typ_struct typ_glob strings i
            >>= fun i' ->
            make_eexpr_of_ast typ_var typ_fun typ_struct typ_glob strings v
            >>= fun v' ->
            type_expr typ_var typ_fun typ_struct typ_glob e' >>= fun t ->
            type_expr typ_var typ_fun typ_struct typ_glob i' >>= fun t' ->
            type_expr typ_var typ_fun typ_struct typ_glob v' >>= fun t'' ->
            match (t, t', t'') with
            | (Ttab (t, _) | Tptr t), t', t'' when t === t'' && t' === Tint ->
                OK (Istore (Ebinop (Eadd, e', i'), v'))
            | _ ->
                Error
                  (Printf.sprintf
                     "Type error in assignment %s[%s] = %s: %s is %s but \
                      should be %s"
                     (dump_eexpr e') (dump_eexpr i') (dump_eexpr v')
                     (dump_eexpr e') (string_of_typ t) (string_of_typ Tint)))
        | _ ->
            Error
              (Printf.sprintf "Unacceptable ast in make_einstr_of_ast %s"
                 (string_of_ast a)))
    | Node (Ttypedexpr, [ t; StringLeaf id ]) when tree_is_typ t -> (
        typ_of_tree t |> fun t ->
        match t with
        | Tint | Tchar | Tptr _ | Tstruct _ | Ttab _ ->
            Hashtbl.add typ_var id t;
            OK (Idecl (t, id))
        | Tvoid -> Error "Cannot declare void variable")
    | Node (Ttypedexpr, [ t; StringLeaf id; e ]) when tree_is_typ t -> (
        typ_of_tree t |> fun t ->
        make_eexpr_of_ast typ_var typ_fun typ_struct typ_glob strings e
        >>= fun e' ->
        type_expr typ_var typ_fun typ_struct typ_glob e' >>= fun t' ->
        if t =/= t' then
          Error
            (Printf.sprintf
               "Type error in declaration %s : %s is %s but should be %s" id
               (dump_eexpr e') (string_of_typ t') (string_of_typ t))
        else
          match t with
          | Tint | Tchar | Tptr _ | Ttab _ ->
              Hashtbl.add typ_var id t;
              OK (Ideclinit (t, id, e'))
          | Tstruct _ -> Error "Cannot declare and initialize struct variable"
          | Tvoid -> Error "Cannot declare void variable")
    | Node (Tblock, instrs) ->
        list_map_res
          (make_einstr_of_ast typ_var typ_fun typ_struct typ_glob strings
             funrettyp)
          instrs
        >>= fun instrs -> OK (Iblock instrs)
    | Node (Tfuncall, [ StringLeaf fname; Node (Tfuncallargs, args) ]) -> (
        match Hashtbl.find_option typ_fun fname with
        | None -> Error (Printf.sprintf "Function %s not declared" fname)
        | Some (argtypedef, rettype) -> (
            list_map_res
              (make_eexpr_of_ast typ_var typ_fun typ_struct typ_glob strings)
              args
            >>= fun args ->
            list_map_res (type_expr typ_var typ_fun typ_struct typ_glob) args
            >>= fun arg_types ->
            if List.length arg_types <> List.length argtypedef then
              Error
                (Printf.sprintf
                   "Type error in function call %s expected %s got %s" fname
                   (string_of_typ_list argtypedef)
                   (string_of_typ_list arg_types))
            else
              List.for_all2 ( === ) arg_types argtypedef |> function
              | true -> OK (Icall (fname, args))
              | false ->
                  Error
                    (Printf.sprintf
                       "Type error in function call %s expected %s got %s" fname
                       (string_of_typ_list argtypedef)
                       (string_of_typ_list arg_types))))
    | _ ->
        Error
          (Printf.sprintf "Unacceptable ast in make_einstr_of_ast %s"
             (string_of_ast a))
  in
  res

let make_ident (a : tree) : (string * typ) res =
  match a with
  | Node (Ttypedexpr, [ t; StringLeaf s ]) when tree_is_typ t ->
      OK (s, typ_of_tree t)
  | a ->
      Error (Printf.sprintf "make_ident: unexpected AST: %s" (string_of_ast a))

(* [addr_taken_expr e] récolte l’ensemble des
   variables dont l’adresse est prise dans une expression e *)
let rec addr_taken_expr (e : expr) : string Set.t =
  match e with
  | Ebinop (_, e1, e2) -> Set.union (addr_taken_expr e1) (addr_taken_expr e2)
  | Ecall (_, el) ->
      List.fold_left
        (fun acc e -> Set.union acc (addr_taken_expr e))
        Set.empty el
  | Eunop (_, e) | Eload e | Egetfield (e, _) -> addr_taken_expr e
  | Eaddrof (Evar v) | Eglobvar v -> Set.singleton v
  | Evar _ | Eaddrof _ | Eint _ | Echar _ -> Set.empty

(* [addr_taken_instr i] récolte l’ensemble des
   variables dont l’adresse est prise dans une instruction i *)
let rec addr_taken_instr (i : instr) : string Set.t =
  match i with
  | Iassign (_, e) | Ideclinit (_, _, e) | Ireturn e -> addr_taken_expr e
  | Iif (e, i1, i2) ->
      Set.union (addr_taken_expr e)
        (Set.union (addr_taken_instr i1) (addr_taken_instr i2))
  | Iwhile (e, i) -> Set.union (addr_taken_expr e) (addr_taken_instr i)
  | Iblock il ->
      List.fold_left
        (fun acc i -> Set.union acc (addr_taken_instr i))
        Set.empty il
  | Icall (_, el) ->
      List.fold_left
        (fun acc e -> Set.union acc (addr_taken_expr e))
        Set.empty el
  | Istore (e1, e2) | Isetfield (e1, _, e2) ->
      Set.union (addr_taken_expr e1) (addr_taken_expr e2)
  | Idecl _ -> Set.empty

let make_offsets_and_stksz funbody funargs typ_var typ_struct typ_glob :
    ((string, int) Hashtbl.t * int) res =
  addr_taken_instr funbody |> fun vars_in_mem ->
  Hashtbl.keys typ_glob |> Set.of_enum |> fun glob_vars ->
  Set.diff vars_in_mem glob_vars |> fun vars_in_mem ->
  Hashtbl.filter (function Tstruct _ | Ttab _ -> true | _ -> false) typ_var
  |> Hashtbl.keys |> Set.of_enum
  |> fun s ->
  Set.diff s (List.split funargs |> fst |> Set.of_list) |> Set.union vars_in_mem
  |> fun vars_in_mem ->
  Set.fold
    (fun v acc ->
      acc >>= fun (offsets, stksz) ->
      type_var typ_var typ_glob v |> fun t ->
      size_type t typ_struct |> fun sz ->
      Hashtbl.add offsets v stksz;
      OK (offsets, stksz + sz))
    vars_in_mem
    (OK (Hashtbl.create 10, 0))

let make_globdef_of_ast typ_fun typ_struct typ_glob strings (a : tree) :
    (string * efun gdef) option res =
  match a with
  | Node
      ( Tfundef,
        Node (Ttypedexpr, [ t; StringLeaf fname ])
        :: Node (Tfunargs, fargs)
        :: fbody )
    when tree_is_typ t -> (
      typ_of_tree t |> fun t ->
      list_map_res make_ident fargs >>= fun funargs ->
      let typ_var = Hashtbl.create (List.length funargs) in
      Hashtbl.replace typ_fun fname (List.map snd funargs, t);
      List.iter (fun (id, t) -> Hashtbl.add typ_var id t) funargs;
      match fbody with
      | [] -> OK None
      | [ fbody ] ->
          make_einstr_of_ast typ_var typ_fun typ_struct typ_glob strings t fbody
          >>= fun funbody ->
          make_offsets_and_stksz funbody funargs typ_var typ_struct typ_glob
          >>= fun (funvarinmem, funstksz) ->
          OK
            (Some
               ( fname,
                 Gfun
                   {
                     funargs;
                     funbody;
                     funrettyp = t;
                     funvartyp = typ_var;
                     funvarinmem;
                     funstksz;
                   } ))
      | _ ->
          Error
            (Printf.sprintf "make_globdef_of_ast: Expected a Tfundef, got %s."
               (string_of_ast a)))
  | Node (Tstructdef, StringLeaf s :: fields) ->
      list_map_res
        (fun field ->
          match field with
          | Node (Ttypedexpr, [ t; StringLeaf s ]) when tree_is_typ t ->
              OK (s, typ_of_tree t)
          | _ ->
              Error
                (Printf.sprintf
                   "make_globdef_of_ast: Expected a Tfundef, got %s."
                   (string_of_ast a)))
        fields
      >>= fun fields ->
      Hashtbl.add typ_struct s fields;
      OK None
  | Node (Ttypedexpr, [ t; StringLeaf s ]) when tree_is_typ t ->
      typ_of_tree t |> fun t ->
      size_type t typ_struct |> fun sz ->
      Hashtbl.add typ_glob s t;
      OK (Some (s, Gvar (t, Ispace sz)))
  | Node (Ttypedexpr, [ t; StringLeaf s; e ]) when tree_is_typ t -> (
      typ_of_tree t |> fun t ->
      Hashtbl.add typ_glob s t;
      match e with
      | IntLeaf i when t === Tint -> OK (Some (s, Gvar (t, Iint i)))
      | CharLeaf c when t === Tchar -> OK (Some (s, Gvar (t, Ichar c)))
      | StringLiteral str -> (
          match t with
          | Ttab (Tchar, n) when String.length str < n ->
              OK (Some (s, Gvar (t, Istring str)))
          | Tptr Tchar ->
              OK
                (Some
                   ( s,
                     Gvar
                       ( Ttab (Tchar, String.length str + 1),
                         Istring (str ^ "\000") ) ))
          | _ ->
              Error
                "make_globdef_of_ast: Cannot initialize non string like \
                 variable with a string.")
      | _ ->
          Error
            "make_globdef_of_ast: Cannot initialize global variable aside from \
             int or char.")
  | _ ->
      Error
        (Printf.sprintf "make_globdef_of_ast: Expected a Tglobdef, got %s."
           (string_of_ast a))

let make_eprog_of_ast (a : tree) : eprog res =
  match a with
  | Node (Tlistglobdef, l) ->
      let typ_fun = Hashtbl.create (List.length l) in
      let typ_struct = Hashtbl.create (List.length l) in
      let typ_glob = Hashtbl.create (List.length l) in
      let strings = Hashtbl.create 17 in
      Hashtbl.replace typ_fun "print" ([ Tint ], Tvoid);
      Hashtbl.replace typ_fun "print_int" ([ Tint ], Tvoid);
      Hashtbl.replace typ_fun "print_char" ([ Tchar ], Tvoid);
      Hashtbl.replace typ_fun "print_string" ([ Tptr Tchar ], Tvoid);
      list_filter_map_res
        (make_globdef_of_ast typ_fun typ_struct typ_glob strings)
        l
      >>= fun l ->
      Hashtbl.fold
        (fun name value acc ->
          acc >>= fun acc ->
          OK
            ((name, Gvar (Ttab (Tchar, String.length value), Istring value))
            :: acc))
        strings (OK l)
      >>= fun l -> OK (l, typ_struct)
  | _ ->
      Error
        (Printf.sprintf "make_globdef_of_ast: Expected a Tlistglobdef, got %s."
           (string_of_ast a))

let pass_elang ast =
  match make_eprog_of_ast ast with
  | Error msg ->
      record_compile_result ~error:(Some msg) "Elang";
      Error msg
  | OK ep ->
      dump !e_dump dump_e ep (fun file () ->
          add_to_report "e" "E" (Code (file_contents file)));
      OK ep
