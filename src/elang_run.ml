open Elang
open Batteries
open Prog
open Utils
open Builtins
open Elang_gen

let binop_bool_to_int f x y = if f x y then 1 else 0

(* [eval_binop b x y] évalue l'opération binaire [b] sur les arguments [x]
   et [y]. *)
let eval_binop (b : binop) : int -> int -> int =
  match b with
  | Eadd -> ( + )
  | Emul -> ( * )
  | Emod -> ( mod )
  | Exor -> ( lxor )
  | Ediv -> ( / )
  | Esub -> ( - )
  | Eclt -> binop_bool_to_int ( < )
  | Ecle -> binop_bool_to_int ( <= )
  | Ecgt -> binop_bool_to_int ( > )
  | Ecge -> binop_bool_to_int ( >= )
  | Eceq -> binop_bool_to_int ( = )
  | Ecne -> binop_bool_to_int ( <> )
  | Eand -> binop_bool_to_int (fun x y -> x <> 0 && y <> 0)
  | Eor -> binop_bool_to_int (fun x y -> x <> 0 || y <> 0)
  | Eband -> ( land )

(* [eval_unop u x] évalue l'opération unaire [u] sur l'argument [x]. *)
let eval_unop (u : unop) : int -> int =
  match u with Eneg -> ( ~- ) | Enot -> fun x -> if x = 0 then 1 else 0

(* [eval_efun typ_fun typ_struct typ_glob oc ep sp st f fname args] évalue
   la fonction [f] avec le nom [fname] et les arguments [args] dans l'état
   [st]. Renvoie une erreur si besoin. *)

(* [eval_eexpr st e] évalue l'expression [e] dans l'état [st]. Renvoie une
   erreur si besoin. *)
let rec eval_eexpr typ_fun typ_struct typ_glob func (oc : Format.formatter) ep
    (sp : int) st (e : expr) : (int * int state) res =
  Printf.printf "eval_eexpr %s\n" (Elang_print.dump_eexpr e);
  match e with
  | Eint n -> OK (n, st)
  | Echar c -> OK (int_of_char c, st)
  | Evar x -> (
      match Hashtbl.find_option st.env x with
      | Some v -> OK (v, st)
      | None -> (
          match Hashtbl.find_option func.funvarinmem x with
          | Some offset -> (
              type_expr func.funvartyp typ_fun typ_struct typ_glob e
              >>= fun t ->
              match t with
              | Tint | Tchar | Tptr _ ->
                  size_type (Hashtbl.find func.funvartyp x) typ_struct
                  |> fun s ->
                  Mem.read_bytes_as_int st.mem offset s >>= fun v -> OK (v, st)
              | Ttab _ | Tstruct _ -> OK (sp + offset, st)
              | Tvoid -> Error "Evar: Cannot read void variable.\n")
          | None -> Error (Format.sprintf "E: Variable %s not found.\n" x)))
  | Eglobvar x -> (
      match Hashtbl.find_option st.glob_env x with
      | Some addr -> (
          match Hashtbl.find_option typ_glob x with
          | Some t -> (
              match t with
              | Tint | Tchar | Tptr _ ->
                  size_type t typ_struct |> fun s ->
                  Mem.read_bytes_as_int st.mem addr s >>= fun v -> OK (v, st)
              | Ttab _ | Tstruct _ -> OK (addr, st)
              | Tvoid -> Error "Eglobvar: Cannot read void variable.\n")
          | None -> Error (Format.sprintf "E: Variable %s not found.\n" x))
      | None -> Error (Format.sprintf "E: Variable %s not found.\n" x))
  | Eunop (u, e) ->
      eval_eexpr typ_fun typ_struct typ_glob func oc ep sp st e
      >>= fun (v, st) -> OK (eval_unop u v, st)
  | Ebinop (b, e1, e2) -> (
      match b with
      | Eand ->
          eval_eexpr typ_fun typ_struct typ_glob func oc ep sp st e1
          >>= fun (v1, st) ->
          if v1 = 0 then OK (0, st)
          else
            eval_eexpr typ_fun typ_struct typ_glob func oc ep sp st e2
            >>= fun (v2, st) -> if v2 = 0 then OK (0, st) else OK (1, st)
      | Eor ->
          eval_eexpr typ_fun typ_struct typ_glob func oc ep sp st e1
          >>= fun (v1, st) ->
          if v1 = 1 then OK (1, st)
          else
            eval_eexpr typ_fun typ_struct typ_glob func oc ep sp st e2
            >>= fun (v2, st) -> if v2 = 1 then OK (1, st) else OK (0, st)
      | _ -> (
          type_expr func.funvartyp typ_fun typ_struct typ_glob e1 >>= fun t1 ->
          type_expr func.funvartyp typ_fun typ_struct typ_glob e2 >>= fun t2 ->
          eval_eexpr typ_fun typ_struct typ_glob func oc ep sp st e1
          >>= fun (v1, st) ->
          eval_eexpr typ_fun typ_struct typ_glob func oc ep sp st e2
          >>= fun (v2, st) ->
          match (t1, t2) with
          | (Tptr t | Ttab (t, _)), _ ->
              size_type t typ_struct |> fun s ->
              OK (eval_binop b v1 (v2 * s), st)
          | _, Tptr t ->
              size_type t typ_struct |> fun s ->
              OK (eval_binop b (v1 * s) v2, st)
          | _ -> OK (eval_binop b v1 v2, st)))
  | Ecall (fname, args) -> (
      ( List.fold_left
          (fun acc e ->
            acc >>= fun (vargs, st) ->
            eval_eexpr typ_fun typ_struct typ_glob func oc ep sp st e
            >>= fun (v, st) -> OK (vargs @ [ v ], st))
          (OK ([], st))
          args
      >>= fun (vargs, st) ->
        find_function ep fname >>= fun f ->
        eval_efun typ_fun typ_struct typ_glob oc ep (sp + f.funstksz) st f fname
          vargs )
      >>= fun (v, st) ->
      match v with
      | Some v -> OK (v, st)
      | None -> Error (Format.sprintf "E: Function %s did not return.\n" fname))
  | Eaddrof e -> (
      match e with
      | Evar x -> (
          match Hashtbl.find_option func.funvarinmem x with
          | Some offset -> OK (sp + offset, st)
          | None -> Error (Format.sprintf "E: Variable %s not found.\n" x))
      | Eglobvar x -> (
          match Hashtbl.find_option st.glob_env x with
          | Some addr -> OK (addr, st)
          | None -> Error (Format.sprintf "E: Variable %s not found.\n" x))
      | Eload e ->
          eval_eexpr typ_fun typ_struct typ_glob func oc ep sp st e
          >>= fun (addr, st) -> OK (addr, st)
      | _ -> Error "E: Only variables and eloads can be taken the address of.\n"
      )
  | Eload e -> (
      eval_eexpr typ_fun typ_struct typ_glob func oc ep sp st e
      >>= fun (addr, st) ->
      type_expr func.funvartyp typ_fun typ_struct typ_glob e >>= fun t ->
      match t with
      | Tptr t -> (
          match t with
          | Tint | Tchar | Tptr _ ->
              Mem.read_bytes_as_int st.mem addr (size_type t typ_struct)
              >>= fun v -> OK (v, st)
          | Tstruct _ | Ttab _ -> OK (addr, st)
          | Tvoid -> Error "Eload: cannot dereference void variable\n")
      | _ -> Error "Eload: Only pointers can be dereferenced.\n")
  | Egetfield (e, f) -> (
      eval_eexpr typ_fun typ_struct typ_glob func oc ep sp st e
      >>= fun (addr, st) ->
      type_expr func.funvartyp typ_fun typ_struct typ_glob e >>= fun t ->
      match t with
      | Tstruct s -> (
          field_offset typ_struct s f >>= fun offset ->
          addr + offset |> fun addr ->
          field_type typ_struct s f >>= fun t ->
          match t with
          | Tint | Tchar | Tptr _ ->
              size_type t typ_struct |> fun s ->
              Mem.read_bytes_as_int st.mem addr s >>= fun v -> OK (v, st)
          | Tstruct _ | Ttab _ -> OK (addr, st)
          | Tvoid -> Error "Egetfield: cannot get void variable\n")
      | _ -> Error "Egetfield: Only structs can be accessed.\n")

(* [eval_einstr oc st ins] évalue l'instrution [ins] en partant de l'état [st].

   Le paramètre [oc] est un "output channel", dans lequel la fonction "print"
   écrit sa sortie, au moyen de l'instruction [Format.fprintf].

   Cette fonction renvoie [(ret, st')] :

   - [ret] est de type [int option]. [Some v] doit être renvoyé lorsqu'une
   instruction [return] est évaluée. [None] signifie qu'aucun [return] n'a eu
   lieu et que l'exécution doit continuer.

   - [st'] est l'état mis à jour. *)
and eval_einstr typ_fun typ_struct typ_glob func oc ep (sp : int)
    (st : int state) (ins : instr) : (int option * int state) res =
  match ins with
  | Idecl _ -> OK (None, st)
  | Ideclinit (_, x, e) | Iassign (x, e) -> (
      eval_eexpr typ_fun typ_struct typ_glob func oc ep sp st e
      >>= fun (v, st) ->
      if is_glob_var func.funvartyp typ_glob x then
        match Hashtbl.find_option st.glob_env x with
        | Some addr ->
            Hashtbl.find typ_glob x |> fun t ->
            size_type t typ_struct |> fun sz ->
            split_bytes sz v |> fun bytes ->
            Mem.write_bytes st.mem addr bytes >>= fun _ -> OK (None, st)
        | None ->
            Error
              (Format.sprintf
                 "E: Variable %s not found in global environment.\n" x)
      else
        match Hashtbl.find_option func.funvarinmem x with
        | Some offset ->
            Hashtbl.find func.funvartyp x |> fun t ->
            size_type t typ_struct |> fun sz ->
            split_bytes sz v |> fun bytes ->
            Mem.write_bytes st.mem (sp + offset) bytes >>= fun _ -> OK (None, st)
        | None ->
            Hashtbl.replace st.env x v;
            OK (None, st))
  | Iif (e, i1, i2) ->
      eval_eexpr typ_fun typ_struct typ_glob func oc ep sp st e
      >>= fun (v, st) ->
      if v = 1 then eval_einstr typ_fun typ_struct typ_glob func oc ep sp st i1
      else eval_einstr typ_fun typ_struct typ_glob func oc ep sp st i2
  | Iwhile (e, i) ->
      eval_eexpr typ_fun typ_struct typ_glob func oc ep sp st e
      >>= fun (v, st) ->
      if v <> 0 then
        eval_einstr typ_fun typ_struct typ_glob func oc ep sp st i
        >>= fun (ret, st') ->
        if ret <> None then OK (ret, st')
        else eval_einstr typ_fun typ_struct typ_glob func oc ep sp st' ins
      else OK (None, st)
  | Iblock is ->
      List.fold_left
        (fun acc i ->
          acc >>= fun (ret, st') ->
          if ret <> None then OK (ret, st')
          else eval_einstr typ_fun typ_struct typ_glob func oc ep sp st' i)
        (OK (None, st))
        is
  | Ireturn e ->
      eval_eexpr typ_fun typ_struct typ_glob func oc ep sp st e
      >>= fun (v, st) -> OK (Some v, st)
  | Icall (fname, args) -> (
      List.fold_left
        (fun acc e ->
          acc >>= fun (vargs, st) ->
          eval_eexpr typ_fun typ_struct typ_glob func oc ep sp st e
          >>= fun (v, st) -> OK (vargs @ [ v ], st))
        (OK ([], st))
        args
      >>= fun (vargs, st) ->
      match find_function ep fname with
      | Error _ -> do_builtin oc st.mem fname vargs >>= fun ret -> OK (ret, st)
      | OK f ->
          eval_efun typ_fun typ_struct typ_glob oc ep (sp + f.funstksz) st f
            fname vargs
          >>= fun (_, st) -> OK (None, st))
  | Istore (e1, e2) -> (
      eval_eexpr typ_fun typ_struct typ_glob func oc ep sp st e1
      >>= fun (addr, st) ->
      eval_eexpr typ_fun typ_struct typ_glob func oc ep sp st e2
      >>= fun (v, st) ->
      type_expr func.funvartyp typ_fun typ_struct typ_glob e1 >>= fun t ->
      match t with
      | Tptr t ->
          split_bytes (size_type t typ_struct) v |> Mem.write_bytes st.mem addr
          >>= fun mem -> OK (None, st)
      | _ ->
          Error
            (Printf.sprintf "E: Only pointers can be dereferenced. %s <- %s\n"
               (Elang_print.dump_eexpr e1)
               (Elang_print.dump_eexpr e2)))
  | Isetfield (e, f, v) -> (
      eval_eexpr typ_fun typ_struct typ_glob func oc ep sp st e
      >>= fun (addr, st) ->
      type_expr func.funvartyp typ_fun typ_struct typ_glob e >>= fun t ->
      eval_eexpr typ_fun typ_struct typ_glob func oc ep sp st v
      >>= fun (value, st) ->
      type_expr func.funvartyp typ_fun typ_struct typ_glob v >>= fun t' ->
      match t with
      | Tstruct s ->
          field_offset typ_struct s f >>= fun offset ->
          split_bytes (size_type t' typ_struct) value |> fun bytes ->
          addr + offset |> fun addr ->
          Mem.write_bytes st.mem addr bytes >>= fun _ -> OK (None, st)
      | _ ->
          Error
            (Printf.sprintf
               "E: Only structs can be accessed. But you tried to access %s.%s \
                which is a %s\n"
               (Elang_print.dump_eexpr e) f (Elang_print.dump_type t)))

(* [eval_efun oc st f fname vargs] évalue la fonction [f] (dont le nom est
   [fname]) en partant de l'état [st], avec les arguments [vargs].

   Cette fonction renvoie un couple (ret, st') avec la même signification que
   pour [eval_einstr]. *)
and eval_efun typ_fun typ_struct typ_glob oc ep (sp : int) (st : int state)
    (func : efun) (fname : string) (vargs : int list) :
    (int option * int state) res =
  (* L'environnement d'une fonction (mapping des variables locales vers leurs
     valeurs) est local et un appel de fonction ne devrait pas modifier les
     variables de l'appelant. Donc, on sauvegarde l'environnement de l'appelant
     dans [env_save], on appelle la fonction dans un environnement propre (Avec
     seulement ses arguments), puis on restore l'environnement de l'appelant. *)
  let env_save = Hashtbl.copy st.env in
  let env = Hashtbl.create 17 in
  match
    List.iter2
      (fun a v -> Hashtbl.replace env a v)
      (List.map fst func.funargs)
      vargs
  with
  | () ->
      eval_einstr typ_fun typ_struct typ_glob func oc ep sp { st with env }
        func.funbody
      >>= fun (v, st') -> OK (v, { st' with env = env_save })
  | exception Invalid_argument _ ->
      Error
        (Format.sprintf
           "E: Called function %s with %d arguments, expected %d.\n" fname
           (List.length vargs) (List.length func.funargs))

(* [eval_eprog oc ep memsize params] évalue un programme complet [ep], avec les
   arguments [params].

   Le paramètre [memsize] donne la taille de la mémoire dont ce programme va
   disposer. Ce n'est pas utile tout de suite (nos programmes n'utilisent pas de
   mémoire), mais ça le sera lorsqu'on ajoutera de l'allocation dynamique dans
   nos programmes.

   Renvoie:

   - [OK (Some v)] lorsque l'évaluation de la fonction a lieu sans problèmes et renvoie une valeur [v].

   - [OK None] lorsque l'évaluation de la fonction termine sans renvoyer de valeur.

   - [Error msg] lorsqu'une erreur survient.
*)

let eval_eprog oc (ep : eprog) (memsize : int) (params : int list) :
    int option res =
  let efuns, typ_struct = ep in
  let st = init_state memsize in
  find_function efuns "main" >>= fun f ->
  (* ne garde que le nombre nécessaire de paramètres pour la fonction "main". *)
  let n = List.length f.funargs in
  let params = take n params in
  let typ_fun = Hashtbl.create (List.length efuns + 3) in
  let typ_glob = Hashtbl.create 17 in
  Hashtbl.replace typ_fun "print" ([ Tint ], Tvoid);
  Hashtbl.replace typ_fun "print_int" ([ Tint ], Tvoid);
  Hashtbl.replace typ_fun "print_char" ([ Tchar ], Tvoid);
  List.fold
    (fun startglob (name, def) ->
      match def with
      | Gvar (t, s) ->
          startglob >>= fun startglob ->
          Hashtbl.replace typ_glob name t;
          init_glob st.mem st.glob_env [ (name, def) ] startglob
      | Gfun func ->
          Hashtbl.add typ_fun name
            (snd (List.split func.funargs), func.funrettyp);
          startglob)
    (OK 0x1000) efuns
  >>= fun _ ->
  List.iter2
    (fun a v -> Hashtbl.replace st.env a v)
    (List.map fst f.funargs) params;
  eval_efun typ_fun typ_struct typ_glob oc efuns 0 st f "main" params
  >>= fun (v, _) -> OK v
