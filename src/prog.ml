open Batteries
open Utils

type mem_access_size = MAS1 | MAS4 | MAS8

type typ =
  | Tint
  | Tchar
  | Tvoid
  | Tptr of typ
  | Tstruct of string
  | Ttab of typ * int

type init_data =
  | Iint of int
  | Ichar of char
  | Ispace of int
  | Istring of string

let rec size_type t typ_struct =
  match t with
  | Tvoid -> 0
  | Tchar -> 1
  | Tint -> Archi.wordsize ()
  | Tptr _ -> Archi.wordsize ()
  | Tstruct s ->
      List.fold_left
        (fun sz (_, t) -> sz + size_type t typ_struct)
        0
        (Hashtbl.find typ_struct s)
  | Ttab (t, l) -> size_type t typ_struct * l

let field_offset (typ_struct : (string, (string * typ) list) Hashtbl.t)
    (s : string) (f : string) : int res =
  match Hashtbl.find_option typ_struct s with
  | None -> Error (Format.sprintf "Unknown struct %s" s)
  | Some sdef ->
      let rec aux acc = function
        | [] -> Error (Format.sprintf "Unknown field %s in struct %s" f s)
        | (f', t) :: tl ->
            if f = f' then OK acc else aux (acc + size_type t typ_struct) tl
      in
      aux 0 sdef

let field_type (structs : (string, (string * typ) list) Hashtbl.t) (s : string)
    (f : string) : typ res =
  match Hashtbl.find_option structs s with
  | None -> Error (Format.sprintf "Unknown struct %s" s)
  | Some sdef ->
      List.assoc_opt f sdef
      ||> Printf.sprintf "Unknown field %s in struct %s" f s

let is_ptr t = match t with Tptr _ -> true | _ -> false
let is_struct t = match t with Tstruct _ -> true | _ -> false

let ( === ) t1 t2 =
  match (t1, t2) with
  | (Tint | Tchar), (Tint | Tchar) -> true
  | Tstruct s, Tstruct s' when s = s' -> true
  | Tptr t, Tptr t' | Tptr t, Ttab (t', _) | Ttab (t', _), Tptr t -> t = t'
  | _ -> false

let ( =/= ) t1 t2 = not (t1 === t2)

let rec string_of_typ t =
  match t with
  | Tint -> "int"
  | Tchar -> "char"
  | Tvoid -> "void"
  | Tptr t -> string_of_typ t ^ "*"
  | Tstruct s -> "struct " ^ s
  | Ttab (t, l) -> string_of_typ t ^ "[" ^ string_of_int l ^ "]"

let string_of_typ_list tl =
  let rec aux acc = function
    | [] -> acc
    | [ t ] -> acc ^ string_of_typ t
    | t :: tl -> aux (acc ^ string_of_typ t ^ ", ") tl
  in
  aux "" tl

let string_of_mem_access_size mas =
  match mas with MAS1 -> "{1}" | MAS4 -> "{4}" | MAS8 -> "{8}"

let mas_of_size n =
  match n with
  | 1 -> OK MAS1
  | 4 -> OK MAS4
  | 8 -> OK MAS8
  | _ -> Error (Printf.sprintf "Unknown memory access size for size = %d" n)

let size_of_mas mas = match mas with MAS1 -> 1 | MAS4 -> 4 | MAS8 -> 8
let archi_mas () = match !Archi.archi with A64 -> MAS8 | A32 -> MAS4

type 'a gdef = Gfun of 'a | Gvar of typ * init_data
type 'a prog = (string * 'a gdef) list

let dump_gdef dump_fun ?dump_glob oc gd =
  (match dump_glob with
  | Some dump_glob -> dump_glob
  | None -> fun oc _ _ _ -> ())
  |> fun f ->
  match gd with
  | fname, Gfun f ->
      dump_fun oc fname f;
      Format.fprintf oc "\n"
  | vname, Gvar (t, d) ->
      f oc vname t d;
      Format.fprintf oc "\n"

let dump_prog dump_fun ?dump_glob oc =
  match dump_glob with
  | Some dump_glob -> List.iter (dump_gdef dump_fun ~dump_glob oc)
  | None -> List.iter (dump_gdef dump_fun oc)

type 'a state = {
  env : (string, 'a) Hashtbl.t;
  mem : Mem.t;
  glob_env : (string, 'a) Hashtbl.t;
}

let init_glob (mem : Mem.t) (glob_env : (string, int) Hashtbl.t) (p : 'a prog)
    (startglob : int) : int res =
  let startglob = ref startglob in
  List.fold
    (fun acc (name, gd) ->
      match gd with
      | Gvar (t, d) -> (
          match d with
          | Iint i ->
              acc >>= fun _ ->
              size_type Tint (Hashtbl.create 0) |> fun int_size ->
              Hashtbl.add glob_env name !startglob;
              Mem.write_bytes mem !startglob (split_bytes int_size i)
              >>= fun _ ->
              startglob := !startglob + int_size;
              OK ()
          | Ichar c ->
              acc >>= fun _ ->
              size_type Tchar (Hashtbl.create 0) |> fun char_size ->
              Hashtbl.add glob_env name !startglob;
              Mem.write_char mem !startglob (int_of_char c) >>= fun _ ->
              startglob := !startglob + char_size;
              OK ()
          | Istring s ->
              acc >>= fun _ ->
              size_type Tchar (Hashtbl.create 0) |> fun char_size ->
              Hashtbl.add glob_env name !startglob;
              list_iter_res
                (fun c ->
                  Mem.write_char mem !startglob (int_of_char c) >>= fun _ ->
                  startglob := !startglob + char_size;
                  OK ())
                (char_list_of_string s)
              >>= fun _ ->
              Printf.printf "startglob = %d\n" !startglob;
              OK ()
          | Ispace n ->
              acc >>= fun _ ->
              Hashtbl.add glob_env name !startglob;
              startglob := !startglob + n;
              OK ())
      | _ -> OK ())
    (OK ()) p
  >>= fun _ -> OK !startglob

let init_state memsize =
  {
    mem = Mem.init memsize;
    env = Hashtbl.create 17;
    glob_env = Hashtbl.create 17;
  }

let set_val env v i = Hashtbl.replace env v i
let get_val env v = Hashtbl.find_option env v

let find_function (ep : 'a prog) fname : 'a res =
  match List.assoc_opt fname (List.rev ep) with
  | Some (Gfun f) -> OK f
  | _ -> Error (Format.sprintf "Unknown function %s\n" fname)
