open Prog
open Elang
open Elang_run
open Batteries
open BatList
open Cfg
open Utils
open Builtins

let ( let* ) = ( >>= )

let rec eval_cfgexpr oc (cp : cprog) sp st (e : expr) :
    (int * int Prog.state) res =
  match e with
  | Ebinop (b, e1, e2) -> (
      match b with
      | Eand ->
          eval_cfgexpr oc cp sp st e1 >>= fun (v1, st) ->
          if v1 = 0 then OK (0, st)
          else
            eval_cfgexpr oc cp sp st e2 >>= fun (v2, st) ->
            if v2 = 0 then OK (0, st) else OK (1, st)
      | Eor ->
          eval_cfgexpr oc cp sp st e1 >>= fun (v1, st) ->
          if v1 <> 0 then OK (1, st)
          else
            eval_cfgexpr oc cp sp st e2 >>= fun (v2, st) ->
            if v2 <> 0 then OK (1, st) else OK (0, st)
      | _ ->
          eval_cfgexpr oc cp sp st e1 >>= fun (v1, st) ->
          eval_cfgexpr oc cp sp st e2 >>= fun (v2, st) ->
          eval_binop b v1 v2 |> fun v -> OK (v, st))
  | Eunop (u, e) ->
      eval_cfgexpr oc cp sp st e >>= fun (v1, st) ->
      eval_unop u v1 |> fun v -> OK (v, st)
  | Eint i -> OK (i, st)
  | Evar s -> (
      match Hashtbl.find_option st.env s with
      | Some v -> OK (v, st)
      | None -> Error (Printf.sprintf "Unknown variable %s\n" s))
  | Eglobvar s -> (
      match Hashtbl.find_option st.glob_env s with
      | Some v -> OK (v, st)
      | None -> Error (Printf.sprintf "Unknown global variable %s\n" s))
  | Ecall (fname, args) -> (
      list_map_res (eval_cfgexpr oc cp sp st) args >>= fun args ->
      match find_function cp fname with
      | OK f ->
          take (List.length f.cfgfunargs) args |> List.map fst |> fun args ->
          eval_cfgfun oc cp (sp + f.cfgfunstksz) st fname f args
          >>= fun (v, st) -> OK (v |> Option.get, st)
      | Error e ->
          do_builtin oc st.mem fname (List.map fst args) >>= fun v ->
          OK (Option.default 0 v, st))
  | Estk i -> OK (sp + i, st)
  | Eload (e, sz) ->
      eval_cfgexpr oc cp sp st e >>= fun (addr, st) ->
      Mem.read_bytes_as_int st.mem addr sz >>= fun value -> OK (value, st)

and eval_cfginstr oc cp sp st ht (n : int) : (int * int state) res =
  match Hashtbl.find_option ht n with
  | None -> Error (Printf.sprintf "Invalid node identifier\n")
  | Some node -> (
      match node with
      | Cnop succ -> eval_cfginstr oc cp sp st ht succ
      | Cassign (v, e, succ) ->
          eval_cfgexpr oc cp sp st e >>= fun (i, st) ->
          Hashtbl.replace st.env v i;
          eval_cfginstr oc cp sp st ht succ
      | Ccmp (cond, i1, i2) ->
          eval_cfgexpr oc cp sp st cond >>= fun (i, st) ->
          if i = 0 then eval_cfginstr oc cp sp st ht i2
          else eval_cfginstr oc cp sp st ht i1
      | Creturn e -> eval_cfgexpr oc cp sp st e >>= fun (e, st) -> OK (e, st)
      | Ccall (fname, args, succ) -> (
          list_map_res (eval_cfgexpr oc cp sp st) args >>= fun args ->
          List.map fst args |> fun args ->
          match find_function cp fname with
          | OK f ->
              take (List.length f.cfgfunargs) args |> fun args ->
              eval_cfgfun oc cp (sp + f.cfgfunstksz) st fname f args
              >>= fun (v, st) -> eval_cfginstr oc cp sp st ht succ
          | Error e ->
              do_builtin oc st.mem fname args >>= fun _ ->
              eval_cfginstr oc cp sp st ht succ)
      | Cstore (e1, e2, sz, succ) ->
          eval_cfgexpr oc cp sp st e1 >>= fun (addr, st) ->
          eval_cfgexpr oc cp sp st e2 >>= fun (value, st) ->
          split_bytes sz value |> fun value ->
          Mem.write_bytes st.mem addr value >>= fun _ ->
          eval_cfginstr oc cp sp st ht succ)

and eval_cfgfun oc cp sp st cfgfunname { cfgfunargs; cfgfunbody; cfgentry }
    vargs =
  let env = Hashtbl.create 17 in
  let st' = { st with env } in
  match
    List.iter2 (fun a v -> Hashtbl.replace st'.env a v) cfgfunargs vargs
  with
  | () ->
      eval_cfginstr oc cp sp st' cfgfunbody cfgentry >>= fun (v, st') ->
      OK (Some v, { st' with env = st.env })
  | exception Invalid_argument _ ->
      Error
        (Format.sprintf
           "CFG: Called function %s with %d arguments, expected %d.\n"
           cfgfunname (List.length vargs) (List.length cfgfunargs))

let eval_cfgprog oc cp memsize (params : int list) =
  let st = init_state memsize in
  find_function cp "main" >>= fun f ->
  let n = List.length f.cfgfunargs in
  let params = take n params in
  List.fold
    (fun startglob (name, def) ->
      match def with
      | Gvar (t, s) ->
          startglob >>= fun startglob ->
          init_glob st.mem st.glob_env [ (name, def) ] startglob
      | Gfun _ -> startglob)
    (OK 0x1000) cp
  >>= fun _ ->
  eval_cfgfun oc cp 0 st "main" f params >>= fun (v, st) -> OK v
