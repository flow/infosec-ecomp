open Batteries
open Elang
open Prog
open Utils

let dump_binop = function
  | Eadd -> Printf.sprintf "+"
  | Esub -> Printf.sprintf "-"
  | Emul -> Printf.sprintf "*"
  | Ediv -> Printf.sprintf "/"
  | Emod -> Printf.sprintf "%%"
  | Exor -> Printf.sprintf "^"
  | Eclt -> Printf.sprintf "<"
  | Ecle -> Printf.sprintf "<="
  | Ecgt -> Printf.sprintf ">"
  | Ecge -> Printf.sprintf ">="
  | Eceq -> Printf.sprintf "=="
  | Ecne -> Printf.sprintf "!="
  | Eand -> Printf.sprintf "&&"
  | Eor -> Printf.sprintf "||"
  | Eband -> Printf.sprintf "&"

let rec dump_type = function
  | Tint -> Printf.sprintf "int"
  | Tchar -> Printf.sprintf "char"
  | Tvoid -> Printf.sprintf "void"
  | Tptr t -> Printf.sprintf "%s*" (dump_type t)
  | Tstruct s -> Printf.sprintf "struct %s" s
  | Ttab (t, i) -> Printf.sprintf "%s[%d]" (dump_type t) i

let dump_unop = function
  | Eneg -> Printf.sprintf "-"
  | Enot -> Printf.sprintf "!"

let rec dump_eexpr = function
  | Ebinop (b, e1, e2) ->
      Printf.sprintf "(%s %s %s)" (dump_eexpr e1) (dump_binop b) (dump_eexpr e2)
  | Eunop (u, e) -> Printf.sprintf "(%s %s)" (dump_unop u) (dump_eexpr e)
  | Eint i -> Printf.sprintf "%d" i
  | Evar s | Eglobvar s -> Printf.sprintf "%s" s
  | Echar c -> Printf.sprintf "'%c'" c
  | Ecall (s, el) ->
      Printf.sprintf "%s(%s)" s (String.concat "," (List.map dump_eexpr el))
  | Eaddrof s -> Printf.sprintf "&%s" (dump_eexpr s)
  | Eload s -> Printf.sprintf "*%s" (dump_eexpr s)
  | Egetfield (e, s) -> Printf.sprintf "%s.%s" (dump_eexpr e) s

let indent_size = 2

let spaces n =
  range (indent_size * n) |> List.map (fun _ -> ' ') |> String.of_list

let print_spaces oc n = Format.fprintf oc "%s" (spaces n)

let rec dump_einstr_rec indent oc i =
  match i with
  | Idecl (t, v) ->
      print_spaces oc indent;
      Format.fprintf oc "%s %s;\n" (dump_type t) v
  | Ideclinit (t, v, e) ->
      print_spaces oc indent;
      Format.fprintf oc "%s %s = %s;\n" (dump_type t) v (dump_eexpr e)
  | Iassign (v, e) ->
      print_spaces oc indent;
      Format.fprintf oc "%s = %s;\n" v (dump_eexpr e)
  | Iif (cond, i1, Iblock []) ->
      print_spaces oc indent;
      Format.fprintf oc "if %s %a\n" (dump_eexpr cond) (dump_einstr_rec indent)
        i1
  | Iif (cond, i1, i2) ->
      print_spaces oc indent;
      Format.fprintf oc "if %s %a else %a\n" (dump_eexpr cond)
        (dump_einstr_rec indent) i1 (dump_einstr_rec indent) i2
  | Iwhile (cond, i) ->
      print_spaces oc indent;
      Format.fprintf oc "while %s %a\n" (dump_eexpr cond)
        (dump_einstr_rec indent) i
  | Iblock il ->
      Format.fprintf oc "{\n";
      List.iter (Format.fprintf oc "%a" (dump_einstr_rec (indent + 1))) il;
      print_spaces oc indent;
      Format.fprintf oc "}"
  | Ireturn e ->
      print_spaces oc indent;
      Format.fprintf oc "return %s;\n" (dump_eexpr e)
  | Icall (s, el) ->
      print_spaces oc indent;
      Format.fprintf oc "%s(%s);\n" s
        (String.concat ", " (List.map dump_eexpr el))
  | Istore (e1, e2) ->
      print_spaces oc indent;
      Format.fprintf oc "*%s = %s;\n" (dump_eexpr e1) (dump_eexpr e2)
  | Isetfield (e1, s, e2) ->
      print_spaces oc indent;
      Format.fprintf oc "%s.%s = %s;\n" (dump_eexpr e1) s (dump_eexpr e2)

let dump_einstr oc i = dump_einstr_rec 0 oc i
let dump_arg (s, t) = Printf.sprintf "%s %s" (dump_type t) s

let dump_efun oc funname { funargs; funbody; funrettyp } =
  if funbody = Iblock [] then
    Format.fprintf oc "%s %s(%s);\n" (dump_type funrettyp) funname
      (String.concat ", " (List.map dump_arg funargs))
  else
    Format.fprintf oc "%s %s(%s) %a\n" (dump_type funrettyp) funname
      (String.concat ", " (List.map dump_arg funargs))
      dump_einstr funbody

let dump_eglob oc varname t d =
  match d with
  | Iint i -> Format.fprintf oc "%s %s = %d;\n" (dump_type t) varname i
  | Ichar c -> Format.fprintf oc "%s %s = '%c';\n" (dump_type t) varname c
  | Istring s -> Format.fprintf oc "%s %s = \"%s\";\n" (dump_type t) varname s
  | Ispace _ -> Format.fprintf oc "%s %s;\n" (dump_type t) varname

let dump_estruct oc sname sfields =
  Format.fprintf oc "struct %s {\n" sname;
  List.iter
    (fun (s, t) -> Format.fprintf oc "  %s %s;\n" (dump_type t) s)
    sfields;
  Format.fprintf oc "};\n\n"

let dump_eprog oc = dump_prog dump_efun ~dump_glob:dump_eglob oc

let dump_e oc p =
  Hashtbl.iter (fun s f -> dump_estruct oc s f) (snd p);
  dump_eprog oc (fst p)
