open Prog

type binop =
  | Eadd
  | Emul
  | Emod
  | Exor
  | Ediv
  | Esub (* binary operations *)
  | Eclt
  | Ecle
  | Ecgt
  | Ecge
  | Eceq
  | Ecne (* comparisons *)
  | Eand
  | Eor (* logical operations *)
  | Eband (* bitwise operations *)

type unop = Eneg | Enot

type expr =
  | Ebinop of binop * expr * expr
  | Eunop of unop * expr
  | Eint of int
  | Evar of string
  | Echar of char
  | Ecall of string * expr list
  | Eaddrof of expr
  | Eload of expr
  | Egetfield of expr * string
  | Eglobvar of string

type instr =
  | Idecl of typ * string
  | Ideclinit of typ * string * expr
  | Iassign of string * expr
  | Isetfield of expr * string * expr
  | Iif of expr * instr * instr
  | Iwhile of expr * instr
  | Iblock of instr list
  | Ireturn of expr
  | Icall of string * expr list
  | Istore of expr * expr

type efun = {
  funargs : (string * typ) list;
  funbody : instr;
  funvartyp : (string, typ) Hashtbl.t;
  funrettyp : typ;
  funvarinmem : (string, int) Hashtbl.t;
  funstksz : int;
}

type eprog = efun prog * (string, (string * typ) list) Hashtbl.t
