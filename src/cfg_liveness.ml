open Batteries
open Cfg

(* Analyse de vivacité *)

(* [vars_in_expr e] renvoie l'ensemble des variables qui apparaissent dans [e]. *)
let rec vars_in_expr (e : expr) =
  match e with
  | Eint _ | Estk _ -> Set.empty
  | Evar v | Eglobvar v -> Set.singleton v
  | Eunop (_, e) | Eload (e, _) -> vars_in_expr e
  | Ebinop (_, e1, e2) -> Set.union (vars_in_expr e1) (vars_in_expr e2)
  | Ecall (f, es) ->
      List.fold_left (fun s e -> Set.union s (vars_in_expr e)) Set.empty es

(* [live_cfg_node node live_after] renvoie l'ensemble des variables vivantes
   avant un nœud [node], étant donné l'ensemble [live_after] des variables
   vivantes après ce nœud. *)
let live_cfg_node (node : cfg_node) (live_after : string Set.t) =
  match node with
  | Cnop _ -> live_after
  | Cassign (id, exp, _) ->
      Set.union (vars_in_expr exp) (Set.remove id live_after)
  | Creturn exp | Ccmp (exp, _, _) -> Set.union (vars_in_expr exp) live_after
  | Ccall (id, exps, _) ->
      List.map vars_in_expr exps
      |> List.fold_left Set.union Set.empty
      |> Set.union (Set.remove id live_after)
  | Cstore (e1, e2, _, _) ->
      Set.union (vars_in_expr e1) (vars_in_expr e2) |> Set.union live_after

(* [live_after_node cfg n] renvoie l'ensemble des variables vivantes après le
   nœud [n] dans un CFG [cfg]. [lives] est l'état courant de l'analyse,
   c'est-à-dire une table dont les clés sont des identifiants de nœuds du CFG et
   les valeurs sont les ensembles de variables vivantes avant chaque nœud. *)
let live_after_node cfg n (lives : (int, string Set.t) Hashtbl.t) : string Set.t
    =
  Set.fold
    (fun succ -> Set.union (Hashtbl.find_default lives succ Set.empty))
    (succs cfg n) Set.empty

(* [live_cfg_nodes cfg lives] effectue une itération du calcul de point fixe.

   Cette fonction met à jour l'état de l'analyse [lives] et renvoie un booléen
   qui indique si le calcul a progressé durant cette itération (i.e. s'il existe
   au moins un nœud n pour lequel l'ensemble des variables vivantes avant ce
   nœud a changé). *)
let live_cfg_nodes (cfg : (int, cfg_node) Hashtbl.t)
    (lives : (int, string Set.t) Hashtbl.t) =
  List.sort compare (Hashtbl.to_list cfg)
  |> List.rev
  |> List.fold_left
       (fun changed (n, node) ->
         Hashtbl.find_default lives n Set.empty |> fun live_before_old ->
         live_after_node cfg n lives |> live_cfg_node node
         |> fun live_before_new ->
         Hashtbl.replace lives n live_before_new;
         changed || not (Set.equal live_before_new live_before_old))
       false

(* [live_cfg_fun f] calcule l'ensemble des variables vivantes avant chaque nœud
   du CFG en itérant [live_cfg_nodes] jusqu'à ce qu'un point fixe soit atteint.
*)
let live_cfg_fun (f : cfg_fun) : (int, string Set.t) Hashtbl.t =
  let lives = Hashtbl.create 17 in
  while live_cfg_nodes f.cfgfunbody lives do
    ()
  done;
  lives
