open Batteries
open Elang
open Cfg
open Rtl
open Prog
open Utils
open Report
open Rtl_print
open Options

(* Une partie de la génération de RTL consiste à allouer les variables dans des
   pseudo-registres RTL.

   Ces registres sont en nombre illimité donc ce problème est facile.

   Étant donnés :
   - [next_reg], le premier numéro de registre disponible (pas encore alloué à
   une variable)
   - [var2reg], une liste d'associations dont les clés sont des variables et les
   valeurs des numéros de registres
   - [v] un nom de variable (de type [string]),

   [find_var (next_reg, var2reg) v] renvoie un triplet [(r, next_reg, var2reg)]:

   - [r] est le registre RTL associé à la variable [v]
   - [next_reg] est le nouveau premier registre disponible
   - [var2reg] est la nouvelle association nom de variable/registre.
*)
let find_var (next_reg, var2reg) v =
  match List.assoc_opt v var2reg with
  | Some r -> (r, next_reg, var2reg)
  | None -> (next_reg, next_reg + 1, assoc_set var2reg v next_reg)

(* [rtl_instrs_of_cfg_expr (next_reg, var2reg) e] construit une liste
   d'instructions RTL correspondant à l'évaluation d'une expression E.

   Le retour de cette fonction est un quadruplet [(r,l,next_reg,var2reg)], où :
   - [r] est le registre RTL dans lequel le résultat de l'évaluation de [e] aura
     été stocké
   - [l] est une liste d'instructions RTL.
   - [next_reg] est le nouveau premier registre disponible
   - [var2reg] est la nouvelle association nom de variable/registre.
*)
let rec rtl_instrs_of_cfg_expr n next_label (next_reg, var2reg) (e : expr) =
  match e with
  | Eint i ->
      (next_reg, [ Rconst (next_reg, i) ], next_reg + 1, var2reg, next_label)
  | Evar v ->
      let r, next_reg, var2reg = find_var (next_reg, var2reg) v in
      (r, [], next_reg, var2reg, next_label)
  | Eglobvar v ->
      (next_reg, [ Rglobvar (next_reg, v) ], next_reg + 1, var2reg, next_label)
  | Ebinop (b, e1, e2) -> (
      let r, l1, next_reg, var2reg, next_label =
        rtl_instrs_of_cfg_expr n next_label (next_reg, var2reg) e1
      in
      let r', l2, next_reg, var2reg, next_label =
        rtl_instrs_of_cfg_expr n next_label (next_reg, var2reg) e2
      in
      match b with
      | Eand ->
          ( next_reg,
            l1
            @ [ Rconst (next_reg, 0) ]
            @ [ Rbranch (Rceq, r, next_reg, (n, next_label)) ]
            @ l2
            @ [ Rbinop (Eand, next_reg, r, r'); Rlabel (n, next_label) ],
            next_reg + 1,
            var2reg,
            next_label + 1 )
      | Eor ->
          ( next_reg,
            l1
            @ [ Rconst (next_reg, 1) ]
            @ [ Rbranch (Rceq, r, next_reg, (n, next_label)) ]
            @ l2
            @ [ Rbinop (Eor, next_reg, r, r'); Rlabel (n, next_label) ],
            next_reg + 1,
            var2reg,
            next_label + 1 )
      | _ ->
          ( next_reg,
            l1 @ l2 @ [ Rbinop (b, next_reg, r, r') ],
            next_reg + 1,
            var2reg,
            next_label ))
  | Eunop (u, e) ->
      let r, l, next_reg, var2reg, next_label =
        rtl_instrs_of_cfg_expr n next_label (next_reg, var2reg) e
      in
      ( next_reg,
        l @ [ Runop (u, next_reg, r) ],
        next_reg + 1,
        var2reg,
        next_label )
  | Ecall (id, args) ->
      let cfg_list, args_reg, next_reg', var2reg', next_label =
        List.fold_left
          (fun (cfg_list, args_reg, next_reg', var2reg', next_label) e ->
            let rres, l, next_reg', var2reg', next_label =
              rtl_instrs_of_cfg_expr n next_label (next_reg', var2reg') e
            in
            (cfg_list @ l, args_reg @ [ rres ], next_reg', var2reg', next_label))
          ([], [], next_reg, var2reg, next_label)
          args
      in
      ( next_reg',
        cfg_list @ [ Rcall (Some next_reg', id, args_reg) ],
        next_reg' + 1,
        var2reg',
        next_label )
  | Estk r ->
      (next_reg, [ Rstk (next_reg, r) ], next_reg + 1, var2reg, next_label)
  | Eload (e, i) ->
      let r, l, next_reg, var2reg, next_label =
        rtl_instrs_of_cfg_expr n next_label (next_reg, var2reg) e
      in
      ( next_reg,
        l @ [ Rload (next_reg, r, i) ],
        next_reg + 1,
        var2reg,
        next_label )

let is_cmp_op = function
  | Eclt -> Some Rclt
  | Ecle -> Some Rcle
  | Ecgt -> Some Rcgt
  | Ecge -> Some Rcge
  | Eceq -> Some Rceq
  | Ecne -> Some Rcne
  | _ -> None

let rtl_cmp_of_cfg_expr (e : expr) =
  match e with
  | Ebinop (b, e1, e2) -> (
      match is_cmp_op b with
      | None -> (Rcne, e, Eint 0)
      | Some rop -> (rop, e1, e2))
  | _ -> (Rcne, e, Eint 0)

let rtl_instrs_of_cfg_node n ((next_reg : int), (var2reg : (string * int) list))
    (c : cfg_node) =
  match c with
  | Cassign (id, exp, next) -> (
      match exp with
      | Eint i ->
          let r, next_reg, var2reg = find_var (next_reg, var2reg) id in
          ([ Rconst (r, i); Rjmp (next, 0) ], next_reg, var2reg)
      | _ ->
          let r, l, next_reg, var2reg, _ =
            rtl_instrs_of_cfg_expr n 1 (next_reg, var2reg) exp
          in
          let r', next_reg, var2reg = find_var (next_reg, var2reg) id in
          ( l @ [ Rmov (r', r); Rjmp (next, 0) ],
            next_reg,
            assoc_set var2reg id r' ))
  | Creturn exp ->
      let r, l, next_reg, var2reg, _ =
        rtl_instrs_of_cfg_expr n 1 (next_reg, var2reg) exp
      in
      (l @ [ Rret r ], next_reg, var2reg)
  | Ccmp (exp, next1, next2) ->
      let rop, e1, e2 = rtl_cmp_of_cfg_expr exp in
      let r1, l1, next_reg, var2reg, next_label =
        rtl_instrs_of_cfg_expr n 1 (next_reg, var2reg) e1
      in
      let r2, l2, next_reg, var2reg, _ =
        rtl_instrs_of_cfg_expr n next_label (next_reg, var2reg) e2
      in
      ( l1 @ l2 @ [ Rbranch (rop, r1, r2, (next1, 0)); Rjmp (next2, 0) ],
        next_reg,
        var2reg )
  | Cnop next -> ([ Rjmp (next, 0) ], next_reg, var2reg)
  | Ccall (id, args, next) ->
      let cfg_list, args_reg, next_reg', var2reg', _ =
        List.fold_left
          (fun (cfg_list, args_reg, next_reg', var2reg', next_label) e ->
            let rres, l, next_reg', var2reg', next_label =
              rtl_instrs_of_cfg_expr n next_label (next_reg', var2reg') e
            in
            (cfg_list @ l, args_reg @ [ rres ], next_reg', var2reg', next_label))
          ([], [], next_reg, var2reg, 1)
          args
      in
      ( cfg_list @ [ Rcall (None, id, args_reg); Rjmp (next, 0) ],
        next_reg',
        var2reg' )
  | Cstore (e1, e2, size, next) ->
      let r1, l1, next_reg, var2reg, next_label =
        rtl_instrs_of_cfg_expr n 1 (next_reg, var2reg) e1
      in
      let r2, l2, next_reg, var2reg, _ =
        rtl_instrs_of_cfg_expr n next_label (next_reg, var2reg) e2
      in
      (l1 @ l2 @ [ Rstore (r1, r2, size); Rjmp (next, 0) ], next_reg, var2reg)

let rtl_instrs_of_cfg_fun cfgfunname
    ({ cfgfunargs; cfgfunbody; cfgentry; cfgfunstksz } : cfg_fun) =
  let rargs, next_reg, var2reg =
    List.fold_left
      (fun (rargs, next_reg, var2reg) a ->
        let r, next_reg, var2reg = find_var (next_reg, var2reg) a in
        (rargs @ [ r ], next_reg, var2reg))
      ([], 0, []) cfgfunargs
  in
  let rtlfunbody = Hashtbl.create 17 in
  let next_reg, var2reg =
    Hashtbl.fold
      (fun n node (next_reg, var2reg) ->
        let l, next_reg, var2reg =
          rtl_instrs_of_cfg_node n (next_reg, var2reg) node
        in
        Hashtbl.replace rtlfunbody n l;
        (next_reg, var2reg))
      cfgfunbody (next_reg, var2reg)
  in
  {
    rtlfunargs = rargs;
    rtlfunentry = cfgentry;
    rtlfunbody;
    rtlfuninfo = var2reg;
    rtlfunstksz = cfgfunstksz;
  }

let rtl_of_gdef funname = function
  | Gfun f -> Gfun (rtl_instrs_of_cfg_fun funname f)
  | Gvar (t, v) -> Gvar (t, v)

let rtl_of_cfg cp = List.map (fun (s, gd) -> (s, rtl_of_gdef s gd)) cp

let pass_rtl_gen cfg =
  let rtl = rtl_of_cfg cfg in
  dump !rtl_dump dump_rtl_prog rtl (fun file () ->
      add_to_report "rtl" "RTL" (Code (file_contents file)));
  OK rtl
