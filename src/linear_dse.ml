open Rtl
open Linear
open Linear_liveness
open Batteries
open BatList
open Prog
open Utils
open Report
open Linear_print
open Report
open Options

let dse_instr (ins : rtl_instr) live =
  match ins with
  | Rcall (None, _, _) | Rlabel _ | Rret _ | Rstore _ | Rjmp _ | Rbranch _ ->
      [ ins ]
  | Rmov (rd, _)
  | Rstk (rd, _)
  | Rconst (rd, _)
  | Runop (_, rd, _)
  | Rbinop (_, rd, _, _)
  | Rload (rd, _, _)
  | Rglobvar (rd, _) ->
      if Set.mem rd live then [ ins ] else []
  | Rcall (Some rd, name, args) ->
      if Set.mem rd live then [ ins ] else [ Rcall (None, name, args) ]

let dse_fun live { linearfunargs; linearfunbody; linearfuninfo; linearfunstksz }
    =
  let body =
    linearfunbody
    |> List.mapi (fun i ins ->
           dse_instr ins (Hashtbl.find_default live i Set.empty))
    |> List.concat
  in
  { linearfunargs; linearfunbody = body; linearfuninfo; linearfunstksz }

let dse_prog p live =
  if !Options.no_linear_dse then p
  else
    List.map
      (fun (name, gdef) ->
        match gdef with
        | Gfun f ->
            let live =
              Hashtbl.find_default live name
                (Hashtbl.create 17, Hashtbl.create 17)
              |> snd
            in
            let f = dse_fun live f in
            (name, Gfun f)
        | Gvar (v, init) -> (name, Gvar (v, init)))
      p

let pass_linear_dse linear lives =
  let linear = dse_prog linear lives in
  let newlives = liveness_linear_prog linear in
  Hashtbl.iter (fun k v -> Hashtbl.replace lives k v) newlives;
  record_compile_result "DSE";
  dump
    (!linear_dump >*> fun s -> s ^ "1")
    (fun oc -> dump_linear_prog oc (Some lives))
    linear
    (fun file () ->
      add_to_report "linear-after-dse" "Linear after DSE"
        (Code (file_contents file)));
  OK linear
