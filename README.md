# E Compiler

This is a compiler for the E programming language. Written for the compilation course at CentraleSupélec. The compiler is written in OCaml and is built using the following architecture:
![Architecture](./compiler%20architecture.png)

## Features

At the moment, the compiler supports the following features of the E language:

- Functions
- Types (int, char, pointer, array, struct)
- Structures (passing by reference only)
- Pointers
- Arrays
- Logical operators (&&, ||, !)
- Bitwise operators (& only for now)
- loops (while, for)
- increment/decrement (++, -- not in expressions)
- array arguments (int *a, int a[])

## Optimizations

The compiler performs the following optimization passes:

- cfg level optimizations:
  - Constant evaluation (no propagation yet)
  - Dead assignment elimination (for local variables only)
- linear optimizations:
  - Dead store elimination

## Todo

- Remaining bitwise operators (|, ^, ~)
- String literals (char * and char [])

## Play SPACE INVADERS

Requires docker and tightvncviewer.

At the root of the project,

```bash
make invader

docker run --rm -v $(pwd):/data -p 5900:5900 -it pwilke/qemu-compil invader
```

Then, in another terminal,

```bash
vncviewer 127.0.0.1:5900
```

## Acknowledgements

The subject of the project was provided by the course staff of the compilation course at CentraleSupélec:

- Pierre Wilke
- Frédéric Tronel
