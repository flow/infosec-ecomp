tokens SYM_EOF SYM_IDENTIFIER<string> SYM_INTEGER<int> SYM_CHARACTER<char> SYM_STRING<string>
tokens SYM_PLUS SYM_MINUS SYM_ASTERISK SYM_DIV SYM_MOD
tokens SYM_LPARENTHESIS SYM_RPARENTHESIS SYM_LBRACE SYM_RBRACE SYM_LBRACKET SYM_RBRACKET
tokens SYM_ASSIGN SYM_SEMICOLON SYM_RETURN SYM_IF SYM_WHILE SYM_ELSE SYM_COMMA SYM_POINT
tokens SYM_INT SYM_VOID SYM_CHAR SYM_ARROW SYM_FOR
tokens SYM_EQUALITY SYM_NOTEQ SYM_LT SYM_LEQ SYM_GT SYM_GEQ
tokens SYM_AMPERSAND SYM_STRUCT SYM_BOOL_AND SYM_BOOL_OR SYM_BOOL_NOT
non-terminals S INSTR INSTRS LINSTRS ELSE EXPR FACTOR
non-terminals LPARAMS REST_PARAMS
non-terminals IDENTIFIER INTEGER CHARACTER STRING
non-terminals DEFS FUNDEF_OR_STRUCTDEF_OR_GLOBVARDEF STRUCTBODY
non-terminals FUNCALLPARAMS LFUNPARAMS LFUNRESTPARAMS
non-terminals FUNCALL_OR_ASSIGN ASSIGN FUNCALL_OR_VAR_OR_MEMBER_OR_INDEX VAR_OR_MEMBER_OR_INDEX
non-terminals LOR_EXPRS LOR_EXPR
non-terminals LAND_EXPRS LAND_EXPR
non-terminals BAND_EXPRS BAND_EXPR
non-terminals EQ_EXPRS EQ_EXPR
non-terminals CMP_EXPRS CMP_EXPR
non-terminals ADD_EXPRS ADD_EXPR
non-terminals MUL_EXPRS MUL_EXPR
non-terminals TYPE_SPECIFIER POINTER_OR_NOTHING BRACKETS_OR_NOTHING
non-terminals DECLARATION ASSIGN_OR_NOTHING LINSTRS_OR_SEMICOLON ARRAY_OR_NOTHING FUNDEF_OR_ASSIGN_OR_NOTHING
axiom S
{

  open Symbols
  open Ast
  open BatPrintf
  open BatBuffer
  open Batteries
  open Utils

  let resolve_associativity term other =
    List.fold_left (fun acc (tag, tree) -> Node(tag,acc::tree)) term other


}

rules
S -> DEFS SYM_EOF {  Node (Tlistglobdef, $1) }
LINSTRS -> SYM_LBRACE INSTRS SYM_RBRACE {Node(Tblock, $2)}

DEFS ->  {[]}
DEFS -> TYPE_SPECIFIER FUNDEF_OR_STRUCTDEF_OR_GLOBVARDEF DEFS { $2 $1 :: $3 }

FUNDEF_OR_STRUCTDEF_OR_GLOBVARDEF -> IDENTIFIER FUNDEF_OR_ASSIGN_OR_NOTHING {$2 $1}
FUNDEF_OR_STRUCTDEF_OR_GLOBVARDEF -> SYM_LBRACE STRUCTBODY SYM_RBRACE SYM_SEMICOLON {
  fun t ->
    match t with
    | Node(Tstruct, [s]) -> Node(Tstructdef, s :: $2)
    | _ -> failwith "structdef"
}

FUNDEF_OR_ASSIGN_OR_NOTHING -> SYM_LPARENTHESIS LPARAMS SYM_RPARENTHESIS LINSTRS_OR_SEMICOLON {
  fun funname funrettype  ->
    $4 (Node(Ttypedexpr, [funrettype; funname])) (Node(Tfunargs, $2))
}
FUNDEF_OR_ASSIGN_OR_NOTHING -> ARRAY_OR_NOTHING ASSIGN_OR_NOTHING SYM_SEMICOLON {
  fun varname vartype ->
    $2 ($1 vartype) varname
}

STRUCTBODY -> {[]}
STRUCTBODY -> TYPE_SPECIFIER IDENTIFIER ARRAY_OR_NOTHING SYM_SEMICOLON STRUCTBODY {Node(Ttypedexpr, [$3 $1; $2]) :: $5}

LINSTRS_OR_SEMICOLON -> SYM_SEMICOLON {fun def funargs -> Node(Tfundef, [def; funargs])}
LINSTRS_OR_SEMICOLON -> LINSTRS {fun def funargs -> Node(Tfundef, [def; funargs; $1])}

LPARAMS -> {[]}
LPARAMS -> TYPE_SPECIFIER IDENTIFIER BRACKETS_OR_NOTHING REST_PARAMS {Node(Ttypedexpr, [$3 $1; $2]) :: $4}
BRACKETS_OR_NOTHING -> SYM_LBRACKET SYM_RBRACKET {
  fun t -> Node(Tpointer, [t])
}
BRACKETS_OR_NOTHING -> {fun t -> t}
REST_PARAMS -> SYM_COMMA TYPE_SPECIFIER IDENTIFIER REST_PARAMS {Node(Ttypedexpr, [$2; $3]):: $4}
REST_PARAMS -> {[]}

INSTRS -> {[]}
INSTRS -> INSTR INSTRS {$1::$2}

INSTR -> LINSTRS {$1}
INSTR -> SYM_IF SYM_LPARENTHESIS EXPR SYM_RPARENTHESIS LINSTRS ELSE {Node(Tif, $3::$5::$6)}
INSTR -> SYM_WHILE SYM_LPARENTHESIS EXPR SYM_RPARENTHESIS LINSTRS {Node(Twhile, [$3;$5])}
INSTR -> SYM_FOR SYM_LPARENTHESIS INSTR EXPR SYM_SEMICOLON IDENTIFIER ASSIGN SYM_RPARENTHESIS LINSTRS {
  match $9 with
  | Node(Tblock, instrs) ->
    Node(Tblock, [
      $3;
      Node(Twhile, [$4; Node(Tblock, instrs @ [$7 $6] )])
    ])
  | _ -> failwith "for"
}

INSTR -> SYM_RETURN EXPR SYM_SEMICOLON {Node(Treturn, [$2])}
INSTR -> SYM_ASTERISK EXPR SYM_ASSIGN EXPR SYM_SEMICOLON {Node(Tassign, [Node(Tassignptr, [$2;$4])])}
INSTR -> DECLARATION SYM_SEMICOLON {$1}
INSTR -> IDENTIFIER FUNCALL_OR_ASSIGN SYM_SEMICOLON {$2 $1}
INSTR -> SYM_LPARENTHESIS EXPR SYM_RPARENTHESIS ASSIGN SYM_SEMICOLON {$4 $2}

ASSIGN -> SYM_POINT IDENTIFIER SYM_ASSIGN EXPR {fun id -> Node(Tassign, [Node(Tassignmember,[id;$2;$4])])}
ASSIGN -> SYM_ARROW IDENTIFIER SYM_ASSIGN EXPR {fun id -> Node(Tassign, [Node(Tassignmemberptr,[id;$2;$4])])}
ASSIGN -> SYM_LBRACKET EXPR SYM_RBRACKET SYM_ASSIGN EXPR {fun id -> Node(Tassign, [Node(Tassignindex,[id;$2;$5])])}
ASSIGN -> SYM_ASSIGN EXPR  {fun id -> Node(Tassign, [Node(Tassignvar,[id;$2])])}
ASSIGN -> SYM_PLUS SYM_PLUS {
  fun id ->
    Node(
      Tassign,
      [
        Node(
          Tassignvar,
          [
            id;
            Node (Tadd, [id; IntLeaf 1])
          ]
        )
      ]
    )
}
ASSIGN -> SYM_MINUS SYM_MINUS {
  fun id ->
    Node(
      Tassign,
      [
        Node(
          Tassignvar,
          [
            id;
            Node (Tsub, [id; IntLeaf 1])
          ]
        )
      ]
    )
}


FUNCALL_OR_ASSIGN -> FUNCALLPARAMS {$1}
FUNCALL_OR_ASSIGN -> ASSIGN {$1}

DECLARATION -> TYPE_SPECIFIER IDENTIFIER ARRAY_OR_NOTHING ASSIGN_OR_NOTHING {$4 ($3 $1) $2}
ARRAY_OR_NOTHING -> SYM_LBRACKET INTEGER SYM_RBRACKET {fun t -> Node(Tarray, [t; $2])}
ARRAY_OR_NOTHING -> {fun t -> t}

ASSIGN_OR_NOTHING -> {fun vartype varname -> Node(Ttypedexpr, [vartype; varname])}
ASSIGN_OR_NOTHING -> SYM_ASSIGN EXPR {
  fun vartype varname ->
    Node(Ttypedexpr, [vartype; varname; $2])
}

ELSE -> SYM_ELSE LINSTRS {[$2]}
ELSE -> {[]}

EXPR -> LOR_EXPR LOR_EXPRS {resolve_associativity $1 $2}

LOR_EXPRS -> SYM_BOOL_OR LOR_EXPR LOR_EXPRS {(Tor, [$2]) :: $3}
LOR_EXPRS -> {[]}

LOR_EXPR -> LAND_EXPR LAND_EXPRS {resolve_associativity $1 $2}

LAND_EXPRS -> SYM_BOOL_AND LAND_EXPR LAND_EXPRS {(Tand, [$2])::$3}
LAND_EXPRS -> {[]}

LAND_EXPR -> BAND_EXPR BAND_EXPRS {resolve_associativity $1 $2}

BAND_EXPRS -> SYM_AMPERSAND BAND_EXPR BAND_EXPRS {(Tband, [$2])::$3}
BAND_EXPRS -> {[]}

BAND_EXPR -> EQ_EXPR EQ_EXPRS {resolve_associativity $1 $2}

EQ_EXPRS -> SYM_EQUALITY EQ_EXPR {[(Tceq, [$2])]}
EQ_EXPRS -> SYM_NOTEQ EQ_EXPR {[(Tne, [$2])]}
EQ_EXPRS ->{[]}

EQ_EXPR -> CMP_EXPR CMP_EXPRS {resolve_associativity $1 $2}

CMP_EXPRS -> SYM_LT CMP_EXPR {[(Tclt, [$2])]}
CMP_EXPRS -> SYM_GT CMP_EXPR {[(Tcgt, [$2])]}
CMP_EXPRS -> SYM_LEQ CMP_EXPR {[(Tcle, [$2])]}
CMP_EXPRS -> SYM_GEQ CMP_EXPR {[(Tcge, [$2])]}
CMP_EXPRS -> {[]}

CMP_EXPR -> ADD_EXPR ADD_EXPRS {resolve_associativity $1 $2}

ADD_EXPRS -> SYM_PLUS ADD_EXPR ADD_EXPRS {(Tadd, [$2])::$3}
ADD_EXPRS -> SYM_MINUS ADD_EXPR ADD_EXPRS {(Tsub, [$2])::$3}
ADD_EXPRS -> {[]}

ADD_EXPR -> MUL_EXPR MUL_EXPRS {resolve_associativity $1 $2}

MUL_EXPRS -> SYM_ASTERISK MUL_EXPR MUL_EXPRS {(Tmul, [$2])::$3}
MUL_EXPRS -> SYM_DIV MUL_EXPR MUL_EXPRS {(Tdiv, [$2])::$3}
MUL_EXPRS -> SYM_MOD MUL_EXPR MUL_EXPRS {(Tmod, [$2])::$3}
MUL_EXPRS -> {[]}

MUL_EXPR -> FACTOR {$1}
MUL_EXPR -> SYM_MINUS FACTOR {Node(Tneg,[$2])}
MUL_EXPR -> SYM_BOOL_NOT FACTOR {Node(Tnot,[$2])}

FACTOR -> IDENTIFIER FUNCALL_OR_VAR_OR_MEMBER_OR_INDEX {$2 $1}
FACTOR -> SYM_LPARENTHESIS EXPR SYM_RPARENTHESIS VAR_OR_MEMBER_OR_INDEX {$4 $2}
FACTOR -> CHARACTER {$1}
FACTOR -> INTEGER {$1}
FACTOR -> STRING {$1}
FACTOR -> SYM_AMPERSAND FACTOR {Node(Tref, [$2])}
FACTOR -> SYM_ASTERISK FACTOR {Node(Tvalue, [$2])}

FUNCALL_OR_VAR_OR_MEMBER_OR_INDEX -> FUNCALLPARAMS {$1}
FUNCALL_OR_VAR_OR_MEMBER_OR_INDEX -> VAR_OR_MEMBER_OR_INDEX {$1}

VAR_OR_MEMBER_OR_INDEX -> {fun id -> id}
VAR_OR_MEMBER_OR_INDEX -> SYM_POINT IDENTIFIER {fun id -> Node(Tmember, [id; $2])}
VAR_OR_MEMBER_OR_INDEX -> SYM_ARROW IDENTIFIER {fun id -> Node(Tmemberptr, [id; $2])}
VAR_OR_MEMBER_OR_INDEX -> SYM_LBRACKET EXPR SYM_RBRACKET {fun id -> Node(Tindex, [id; $2])}

IDENTIFIER -> SYM_IDENTIFIER {StringLeaf($1)}
INTEGER -> SYM_INTEGER {IntLeaf($1)}
CHARACTER -> SYM_CHARACTER {CharLeaf($1)}
STRING -> SYM_STRING {StringLiteral($1)}

FUNCALLPARAMS -> SYM_LPARENTHESIS LFUNPARAMS SYM_RPARENTHESIS {fun id -> Node(Tfuncall, [id; Node(Tfuncallargs, $2)])}

LFUNPARAMS -> {[]}
LFUNPARAMS -> EXPR LFUNRESTPARAMS {$1::$2}

LFUNRESTPARAMS -> SYM_COMMA EXPR LFUNRESTPARAMS {$2::$3}
LFUNRESTPARAMS -> {[]}

TYPE_SPECIFIER -> SYM_INT POINTER_OR_NOTHING {$2 (Node(Tint,[]))}
TYPE_SPECIFIER -> SYM_VOID POINTER_OR_NOTHING {$2 (Node(Tvoid,[]))}
TYPE_SPECIFIER -> SYM_CHAR POINTER_OR_NOTHING {$2 (Node(Tchar,[]))}
TYPE_SPECIFIER -> SYM_STRUCT IDENTIFIER POINTER_OR_NOTHING {$3 (Node(Tstruct, [$2]))}

POINTER_OR_NOTHING -> {fun t -> t}
POINTER_OR_NOTHING -> SYM_ASTERISK POINTER_OR_NOTHING {fun t -> $2 (Node(Tpointer, [t]))}
