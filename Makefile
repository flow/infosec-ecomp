all: ecomp

include opts.mk

.PHONY: ecomp

src/config.ml: configure opts.mk
	./configure ${CONF_OPTS}

.PHONY: alpaga
alpaga/alpaga:
	make -C alpaga

src/generated_parser.ml: expr_grammar_action.g alpaga/alpaga
	./alpaga/alpaga \
			-g expr_grammar_action.g \
			-pml src/generated_parser.ml \
			-t grammar.html

ecomp: src/generated_parser.ml src/config.ml
	make -C src
	ln -sf src/_build/default/main.exe ecomp

invader: ecomp
	./ecomp \
			-m32 \
			-f tests/invader/invader.e \
			-nostart \
			-clever-regalloc \
			-no-dot \
			-json /dev/null

	cd tests/invader && \
	$(shell which riscv64-unknown-elf-gcc || which riscv64-unknown-linux-gnu-gcc || which riscv64-linux-gnu-gcc) \
			-march=rv32im -mabi=ilp32 \
			-I. -I./include -g -DENV_QEMU=1 -T ./extended.lds \
			-nostartfiles -nostdlib -nostdinc -static \
			invader.s crt.S setup.c font.c libscreen.c libfemto.a itoa.c \
			-o invader.exe
	ln -sf tests/invader/invader.exe invader

clean:
	make -C alpaga clean
	rm -f src/generated_parser.ml
	rm -f grammar.html
	make -C src clean
	rm -f ecomp
	rm -f invader
	make -C tests clean

test: ecomp
	make -C tests
