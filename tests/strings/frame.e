int term_width;
int term_height;

void clear_screen()
{
    print_string("\x1b[2J");
}

void set_position(int x, int y)
{
    print_string("\x1b[");
    print_int(y);
    print_string(";");
    print_int(x);
    print_string("H");
}

void set_color(int r, int g, int b)
{
    print_string("\x1b[38;2;");
    print_int(r);
    print_string(";");
    print_int(g);
    print_string(";");
    print_int(b);
    print_string("m");
}

void reset_color()
{
    print_string("\x1b[0m");
}

int print_outline()
{
    clear_screen();
    set_position(0, 0);
    set_color(255, 0, 0);

    print_string("┌");
    int i;
    for (i = 0; i < term_width - 2; i = i + 1)
    {
        print_string("─");
    }
    print_string("┐");
    for (i = 0; i < term_height - 2; i = i + 1)
    {
        print_string("\r\n│");
        for (int j = 0; j < term_width - 2; j = j + 1)
        {
            print_string(" ");
        }
        print_string("│");
    }

    print_string("\r\n└");
    for (i = 0; i < term_width - 2; i = i + 1)
    {
        print_string("─");
    }
    print_string("┘");
    reset_color();
}

int strlen(char *str)
{
    int i = 0;
    while (str[i] != 0)
    {
        i = i + 1;
    }
    return i;
}

void print_message(char *msg)
{
    int msg_len = strlen(msg);
    int x = (term_width - msg_len) / 2;
    int y = (term_height - 1) / 2;
    set_position(x, y);
    set_color(0, 255, 0);
    print_string(msg);
    reset_color();
}

void main(int width, int height)
{
    term_width = width;
    term_height = height;
    print_outline();
    print_message("C'est pas mal les boucles for !");
    set_position(0, height + 1);
}
