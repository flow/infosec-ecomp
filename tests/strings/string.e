
char *red = "\x1b[31m";
char *green = "\x1b[32m";
char *blue = "\x1b[34m";

int main()
{
    print_string(red);
    print_string("Hello");
    print_string(green);
    print_string(",");
    print_string(blue);
    print_string(" world");
    print_string(green);
    print_string("!");
    return 0;
}
