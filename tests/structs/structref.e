struct mastruct {
    int a;
    int b;
};

int f(struct mastruct *s) {
    return s->a + s->b;
}

int main() {
    struct mastruct s;
    s.a = 1;
    s.b = 2;
    return f(&s);
}
